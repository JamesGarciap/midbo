<?php

//POST Data

//Contact information


$itn_nombre = $_POST['itn_nombre'];
$itn_organizacion = $_POST['itn_organizacion'];
$itn_email = $_POST['itn_email'];
$itn_telefono = $_POST['itn_telefono'];
$itn_celular = $_POST['itn_celular'];
$itn_comentario = $_POST['itn_comentario'];


//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require '../PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer();
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "james.garcia@webgurus.com.co";
//Password to use for SMTP authentication
$mail->Password = "H34v3n&H3ll";
//Set who the message is to be sent from
$mail->setFrom('james.garcia@webgurus.com.co', 'James Garcia');
//Set who the message is to be sent to
$mail->addAddress('itineranciamidbo@gmail.com', 'Itinerancia Midbo');
//Set the subject line
$mail->Subject = 'Informacion Itinerancia: '.$itn_nombre;

//Cuerpo del mensaje basado en ./mailGeneralTemplate.php
$message = '<html><body>';
$message .= '<div style="width: 640px; font-family: Arial, Helvetica, sans-serif; ">';

//Titulo Principal
$message .= '<h3 style="margin-left:20px;color:#0076A5;">INFORMACIÓN</h3>';
//Inicio Tabla de información familiar
$message .= '<table rules="all" style="border-color: #666;margin-left:20px;width:550px;font-size: 11px;" cellpadding="10">';
//Celdas por defecto
$message .= "<tr><td><strong>Nombre:</strong> </td><td>".$itn_nombre."</td></tr>";
$message .= "<tr><td><strong>Organización:</strong> </td><td>".$itn_organizacion."</td></tr>";
$message .= "<tr><td><strong>Correo:</strong> </td><td>".$itn_email."</td></tr>";
$message .= "<tr><td><strong>Telefono:</strong> </td><td>".$itn_telefono."</td></tr>";
$message .= "<tr><td><strong>Celular:</strong> </td><td>".$itn_celular."</td></tr>";
$message .= "</table>";
//Fin Tabla

 //HTML para parrafos
 $message .= '<div style="width: 550px; margin-left:25px;text-aling:justify; ">';
 $message .= "<p style='font-size: 11px;'>".$itn_comentario.".</p>";
 $message .= "</div>";
 //Fin Parrafos

$message .= "</div>";
$message .= "</body></html>";


//Read an HTML message body from an external file, convert referenced images to embedded,
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
$mail->msgHTML($message);
//Replace the plain text body with one created manually
//$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.gif');

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} 
/*else {

    $location = "location: ./../../../index.php?contacto=1";
	header($location);
	exit;
}
*/


?>