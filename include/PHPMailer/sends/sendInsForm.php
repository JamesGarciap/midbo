<?php

//POST Data

//Contact information

$ins_t_original = $_POST['ins_t_original'];
$ins_t_espanol = $_POST['ins_t_espanol'];
$ins_duracion = $_POST['ins_duracion'];
$ins_a_produccion = $_POST['ins_a_produccion'];
$ins_idioma = $_POST['ins_idioma'];
$ins_subtitulos = $_POST['ins_subtitulos'];
$ins_formato_original = $_POST['ins_formato_original'];
$ins_formato_exhibicion = $_POST['ins_formato_exhibicion'];

$ins_directores = $_POST['ins_directores'];
$ins_productores = $_POST['ins_productores'];
$ins_fotografia = $_POST['ins_fotografia'];
$ins_sonido = $_POST['ins_sonido'];
$ins_montaje = $_POST['ins_montaje'];
$ins_musica = $_POST['ins_musica'];
$ins_otros = $_POST['ins_otros'];
$ins_sinopsis = $_POST['ins_sinopsis'];
$ins_premios = $_POST['ins_premios'];


$ins_nombre = $_POST['ins_nombre'];
$ins_cargo = $_POST['ins_cargo'];
$ins_identificacion = $_POST['ins_identificacion'];
$ins_direccion = $_POST['ins_direccion'];
$ins_ciudad = $_POST['ins_ciudad'];
$ins_pais = $_POST['ins_pais'];
$ins_telefono = $_POST['ins_telefono'];
$ins_celular = $_POST['ins_celular'];
$ins_email = $_POST['ins_email'];




//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require '../PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer();
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "james.garcia@webgurus.com.co";
//Password to use for SMTP authentication
$mail->Password = "H34v3n&H3ll";
//Set who the message is to be sent from
$mail->setFrom('james.garcia@webgurus.com.co', 'James Garcia');
//Set who the message is to be sent to
$mail->addAddress('convocatoriamidbo@gmail.com', 'Convocatoria Midbo');
//Set the subject line
$mail->Subject = 'Nueva Obra inscrita: '.$ins_t_original;

//Cuerpo del mensaje basado en ./mailGeneralTemplate.php
$message = '<html><body>';
$message .= '<div style="width: 640px; font-family: Arial, Helvetica, sans-serif; ">';

//Titulo Principal
$message .= '<h3 style="margin-left:20px;color:#0076A5;">INFORMACIÓN DE LA OBRA</h3>';
//Inicio Tabla de información familiar
$message .= '<table rules="all" style="border-color: #666;margin-left:20px;width:550px;font-size: 11px;" cellpadding="10">';
//Celdas por defecto
$message .= "<tr><td><strong>Título original:</strong> </td><td>".$ins_t_original."</td></tr>";
$message .= "<tr><td><strong>Título en Español:</strong> </td><td>".$ins_t_espanol."</td></tr>";
$message .= "<tr><td><strong>Duración:</strong> </td><td>".$ins_duracion."</td></tr>";
$message .= "<tr><td><strong>Año de Producción:</strong> </td><td>".$ins_a_produccion."</td></tr>";
$message .= "<tr><td><strong>Idioma original:</strong> </td><td>".$ins_idioma."</td></tr>";
$message .= "<tr><td><strong>Subtítulos:</strong> </td><td>".$ins_subtitulos."</td></tr>";
$message .= "<tr><td><strong>Formato original:</strong> </td><td>".$ins_formato_original."</td></tr>";
$message .= "<tr><td><strong>Formato de exhibición:</strong> </td><td>".$ins_formato_exhibicion."</td></tr>";
$message .= "</table>";
//Fin Tabla


//Titulo Principal
$message .= '<h3 style="margin-left:20px;color:#0076A5;">FICHA TÉCNICA</h3>';
//Inicio Tabla de información familiar
$message .= '<table rules="all" style="border-color: #666;margin-left:20px;width:550px;font-size: 11px;" cellpadding="10">';
//Celdas por defecto
$message .= "<tr><td><strong>Director(es):</strong> </td><td>".$ins_directores."</td></tr>";
$message .= "<tr><td><strong>Productor(es):</strong> </td><td>".$ins_productores."</td></tr>";
$message .= "<tr><td><strong>Fotografía:</strong> </td><td>".$ins_fotografia."</td></tr>";
$message .= "<tr><td><strong>Sonido:</strong> </td><td>".$ins_sonido."</td></tr>";
$message .= "<tr><td><strong>Montaje:</strong> </td><td>".$ins_montaje."</td></tr>";
$message .= "<tr><td><strong>Música:</strong> </td><td>".$ins_musica."</td></tr>";
$message .= "<tr><td><strong>Otros:</strong> </td><td>".$ins_otros."</td></tr>";
$message .= "<tr><td><strong>Sinopsis (Máximo 400 caracteres):</strong> </td><td>".$ins_sinopsis."</td></tr>";
$message .= "<tr><td><strong>Premios y festivales (Nombre, Festival, País, Año):</strong> </td><td>".$ins_premios."</td></tr>";
$message .= "</table>";
//Fin Tabla

//Titulo Principal
$message .= '<h3 style="margin-left:20px;color:#0076A5;">INFORMACIÓN DE CONTACTO</h3>';
//Inicio Tabla de información familiar
$message .= '<table rules="all" style="border-color: #666;margin-left:20px;width:550px;font-size: 11px;" cellpadding="10">';
//Celdas por defecto
$message .= "<tr><td><strong>Nombre(es):</strong> </td><td>".$ins_nombre."</td></tr>";
$message .= "<tr><td><strong>Cargo(es):</strong> </td><td>".$ins_cargo."</td></tr>";
$message .= "<tr><td><strong>Identificación:</strong> </td><td>".$ins_identificacion."</td></tr>";
$message .= "<tr><td><strong>Dirección:</strong> </td><td>".$ins_direccion."</td></tr>";
$message .= "<tr><td><strong>Ciudad:</strong> </td><td>".$ins_ciudad."</td></tr>";
$message .= "<tr><td><strong>País:</strong> </td><td>".$ins_pais."</td></tr>";
$message .= "<tr><td><strong>Telefono:</strong> </td><td>".$ins_telefono."</td></tr>";
$message .= "<tr><td><strong>Celular:</strong> </td><td>".$ins_celular."</td></tr>";
$message .= "<tr><td><strong>Correo:</strong> </td><td>".$ins_email."</td></tr>";
$message .= "</table>";
//Fin Tabla




$message .= "</div>";
$message .= "</body></html>";


//Read an HTML message body from an external file, convert referenced images to embedded,
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
$mail->msgHTML($message);
//Replace the plain text body with one created manually
//$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.gif');

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} 
/*else {

    $location = "location: ./../../../index.php?contacto=1";
	header($location);
	exit;
}
*/


?>