-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Servidor: localhost
-- Tiempo de generación: 03-03-2014 a las 18:04:22
-- Versión del servidor: 5.0.51
-- Versión de PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Base de datos: `ralerico`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `banner`
-- 

CREATE TABLE `banner` (
  `id` int(2) NOT NULL auto_increment,
  `imagen` varchar(120) NOT NULL,
  `titulo` varchar(220) NOT NULL,
  `texto` varchar(400) NOT NULL,
  `enlace` varchar(500) NOT NULL,
  `actualizado` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `banner`
-- 

INSERT INTO `banner` VALUES (1, '1.png', 'Primer Banner test', '<p>Fusce ac viverra sapien. In hac habitasse platea dictum. Sed eu ullamcorper sem. Suspendisse non sapien malesuada, pretium erat ac, vehicula eros. test</p>', 'http://www.google.com/test', '2014-03-02 18:48:42');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `categorias`
-- 

CREATE TABLE `categorias` (
  `id` int(2) NOT NULL auto_increment,
  `txt_nombre` varchar(200) NOT NULL,
  `txt_caracteristicas` text NOT NULL,
  `txt_texto` text NOT NULL,
  `imagen` varchar(400) NOT NULL,
  `actualizado` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `categorias`
-- 

INSERT INTO `categorias` VALUES (1, 'Visores Light Shade Fogstop', '<ul>\r\n	<li>Posee cintas adhesivas transparentes para una mejor visibilidad.</li>\r\n	<li>Fijaci&oacute;n a la visera utilizando los sellos adhesivos Raleri que permiten ajustar la posici&oacute;n del visor en caso de equivocarse al instalarlo por primera vez.</li>\r\n	<li>Antiempa&ntilde;ante: cada vez m&aacute;s claro, ideal para la lluvia.</li>\r\n	<li>No genera distorsiones visuales.</li>\r\n	<li>El agua no compromete su funcionamiento.</li>\r\n	<li>Tiene filtro UV 400 de &uacute;ltima generaci&oacute;n.</li>\r\n	<li>Totalmente sellada: no m&aacute;s humedad entre el inserto y la careta. No necesita de limpieza interna.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>', '<p><span style="font-size: 13px;">Light Shade es una l&iacute;nea compuesta por cinco visores de distintos colores para los cascos de Motociclistas disponibles en tres tama&ntilde;os que cubren diferentes tipos de viseras en el mercado.</span></p>', '1.2 Smoked Dark A.jpg', '2014-03-02 20:18:31');
INSERT INTO `categorias` VALUES (2, 'Visores Pcshade Fogstop', '<ul>\r\n	<li>Alta eficiencia: debido a una reacci&oacute;n qu&iacute;mica, oscurece m&aacute;s del 50% ante la exposici&oacute;n de la luz solar.</li>\r\n	<li>Acci&oacute;n r&aacute;pida: tarda menos de 4 segundos en cambiar su tonalidad.</li>\r\n	<li>A diferencia de otros productos sensibles a la luz solar, tambi&eacute;n funciona detr&aacute;s de las viseras con filtro UV.</li>\r\n	<li>Fabricada con una tecnolog&iacute;a que alerta cambiando de color cuando el pigmento sensible a la luz solar empieza a decaer.</li>\r\n	<li>Posee cintas adhesivas transparentes para una mejor visibilidad.</li>\r\n	<li>Fijaci&oacute;n a la visera utilizando los sellos adhesivos Raleri que permiten ajustar la posici&oacute;n del visor en caso de equivocarse al instalarlo por primera vez.</li>\r\n	<li>Antiempa&ntilde;ante: cada vez m&aacute;s claro, ideal para la lluvia.</li>\r\n	<li>No genera distorsiones visuales.</li>\r\n	<li>Tiene filtro UV 400 de &uacute;ltima generaci&oacute;n.</li>\r\n	<li>El agua no compromete su funcionamiento.</li>\r\n	<li>Totalmente sellada: no m&aacute;s humedad entre el inserto y la careta. No necesita de limpieza interna.</li>\r\n	<li>En estos v&iacute;deos es posible conocer m&aacute;s sobre la instalaci&oacute;n y funcionamiento de este novedoso producto:</li>\r\n</ul>\r\n\r\n<p style="margin-left: 40px;"><a href="http://www.youtube.com/watch?v=f4V1SlOFHH8">http://www.youtube.com/watch?v=f4V1SlOFHH8</a></p>\r\n\r\n<p style="margin-left: 40px;"><a href="http://www.youtube.com/watch?v=577lvBd8RKU">http://www.youtube.com/watch?v=577lvBd8RKU</a></p>\r\n\r\n<p style="margin-left: 40px;">&nbsp;</p>', '<p><strong>a. &iquest;Es Antiempa&ntilde;ante?&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p><span style="line-height: 1.6em;">El visor posee un antiempa&ntilde;ante de alta calidad, que gracias a su grosor reducido de 0,5 mm y para los materiales de los que est&aacute; fabricado, no induce distorsiones visuales. Las caracter&iacute;sticas absorbentes y de drenaje en ambas superficies (interior y exterior) y el intersticio sellados por su junta adhesiva se sit&uacute;an entre las mejores soluciones contra la humedad.</span></p>\r\n\r\n<p><strong><span style="line-height: 1.6em;">b. &iquest;C&oacute;mo funciona ante la luz del sol?</span></strong></p>\r\n\r\n<p><span style="line-height: 1.6em;">La funci&oacute;n de oscurecimiento viene dada por un pigmento org&aacute;nico que reacciona qu&iacute;micamente con el componente UV de la luz solar (no funciona con luz artificial).</span></p>\r\n\r\n<p><strong><span style="line-height: 1.6em;">c. &iquest;Es sensible a la temperatura?</span></strong></p>\r\n\r\n<p><span style="line-height: 1.6em;">Si, cuando disminuye la temperatura puede alcanzar un efecto de oscurecimiento mayor, pero el cambio es un poco lento. Cuando la temperatura es alta, el cambio de tonalidad es m&aacute;s r&aacute;pido pero alcanza un nivel m&aacute;s bajo de oscurecimiento.</span></p>\r\n\r\n<p><strong><span style="line-height: 1.6em;">d. &iquest;Trabaja detr&aacute;s de la visera de m&iacute; casco?</span></strong></p>\r\n\r\n<p><span style="line-height: 1.6em;">Si, esta tecnolog&iacute;a esta dise&ntilde;ada para trabajar detr&aacute;s de las viseras de casco, tanto aquellos sin tratamiento (la mayor&iacute;a) incluyendo algunas marcas japonesas muy conocidas y con filtro UV380 el efecto de oscurecimiento sigue siendo v&aacute;lido, pero disminuye significativamente en viseras tratadas con filtros UV400.</span></p>\r\n\r\n<p><strong><span style="line-height: 1.6em;">e. &iquest;Cu&aacute;nto tiempo dura el efecto de cambio ante la luz solar?</span></strong></p>\r\n\r\n<p><span style="line-height: 1.6em;">Los materiales utilizados en la fabricaci&oacute;n de los visores PCSHADE FOGSTOP son transitorios y tienen un ciclo de vida que se puede calcular en una temporada de 4 a 6 meses si el visor es utilizado intensamente durante la exposici&oacute;n al sol por varias horas, o varias temporadas si el visor es utilizado ocasionalmente en tus traslados diarios que no impliquen estar conduciendo por varias horas en el d&iacute;a.</span></p>\r\n\r\n<p><strong><span style="line-height: 1.6em;">f. &iquest;C&oacute;mo debo cuidar PCSHADE FOGSTOP cuando no lo uso?</span></strong></p>\r\n\r\n<p><span style="line-height: 1.6em;">La duraci&oacute;n puede ser de varios a&ntilde;os si se conserva a una temperatura menor de 25&deg;C, tambi&eacute;n depende de algunos trucos escritos en las instrucciones como por ejemplo: no dejar el producto en el sol o el calor cuando no est&eacute; en uso y no dejar el producto (dentro del casco) encadenado a la motocicleta o dentro de un maletero del veh&iacute;culo durante el verano.</span></p>\r\n\r\n<p><strong><span style="line-height: 1.6em;">g. &iquest;C&oacute;mo voy a notar cuando comienza a decaer el efecto de oscurecimiento?</span></strong></p>\r\n\r\n<p><span style="line-height: 1.6em;">Cuando el producto va a perder sus caracter&iacute;sticas sensibles a la luz solar, comenzar&aacute; una reacci&oacute;n que le har&aacute; cambiar a color rosa, como se aprecia en la imagen adjunta (donde el visor esta siendo estimulado con una l&aacute;mpara UV). Pese a que ya no realice los cambios de tonalidad ante la luz del sol, aun mantiene sus componentes de antiempa&ntilde;ante y de protecci&oacute;n UV.</span></p>\r\n\r\n<p><strong><span style="line-height: 1.6em;">h. Mi visor PCSHADE FOGSTOP a&uacute;n no es de color rosa, pero creo que el efecto de oscurecimiento esta disminuyendo&hellip;</span></strong></p>\r\n\r\n<p><span style="line-height: 1.6em;">Si el visor no es de color rosa est&aacute; funcionando correctamente. La disminuci&oacute;n del efecto de oscurecimiento puede ser por los motivos indicados antes como alta temperatura y tratamientos UV en la visera externa o de la neblina (que filtra los rayos UV especialmente en las zonas de fricci&oacute;n). Pero lo m&aacute;s probable es por un factor de percepci&oacute;n: el visor es tan progresista en su desvanecimiento que despu&eacute;s de alg&uacute;n tiempo la vista se acostumbra a las variaciones. Para darse cuenta de ello te sugerimos abrir de vez en cuando el casco.</span></p>\r\n\r\n<p><strong><span style="line-height: 1.6em;">i. Cara al sol el oscurecimiento parece ser insuficiente...</span></strong></p>\r\n\r\n<p><span style="line-height: 1.6em;">Por lo que sabemos que no existe un dispositivo compatible con la conducci&oacute;n que pueda resolver este problema. En algunas horas del d&iacute;a (atardecer y amanecer), si observas en direcci&oacute;n al sol, ver&aacute;s fuertes reflejos y una amplia neblina molesta incluso a trav&eacute;s de un visor totalmente a oscuras. El objetivo de PCSHADE FOGSTOP es darte un paseo c&oacute;modo y seguro, no pedir cosas imposibles.</span></p>\r\n\r\n<p><strong><span style="line-height: 1.6em;">j. &iquest;Cu&aacute;nto tiene de grosor? &iquest;Cu&aacute;nto pesa?</span></strong></p>\r\n\r\n<p><span style="line-height: 1.6em;">El visor pesa unas decenas de gramos con peque&ntilde;as diferencias entre los distintos modelos y es de 0,5 mm de espesor. La junta adhesiva tiene 0,5 mm para un total de 1 mm en el borde.</span></p>\r\n\r\n<p><strong><span style="line-height: 1.6em;">k. &iquest;Es necesario un mantenimiento especial?</span></strong></p>\r\n\r\n<p><span style="line-height: 1.6em;">No, como se instala en una zona natural protegida del casco, nunca necesita cuidados especialmente despu&eacute;s de la instalaci&oacute;n. Se puede limpiar con un pa&ntilde;o de microfibra, o con el&nbsp;</span></p>', '2. VISORES PCSHADE FOGSTOP C.jpg', '2014-03-02 20:26:17');
INSERT INTO `categorias` VALUES (3, 'Visores Lc Drive', '<p><strong>a. &iquest;C&oacute;mo funciona?</strong></p>\r\n\r\n<p>El efecto de oscurecimiento est&aacute; dado por el l&iacute;quido en la celda de LCD, cuyas mol&eacute;culas cambian de direcci&oacute;n cuando son estimuladas el&eacute;ctricamente.</p>\r\n\r\n<p><strong>b. Y si se rompe el l&iacute;quido&hellip;</strong></p>\r\n\r\n<p>LC Drive est&aacute; lleno de vac&iacute;o: en caso de rotura el l&iacute;quido no sale, pero es posible que ingrese aire haciendo que el l&iacute;quido se concentre en puntos del visor.</p>\r\n\r\n<p><strong>c. &iquest;C&oacute;mo se adhiere LC Drive en la visera del casco?</strong></p>\r\n\r\n<p>A trav&eacute;s de un sello adhesivo no permanente, por lo que se proporciona un repuesto en la caja, despu&eacute;s de alg&uacute;n tiempo de uso, se puede comprar de una pieza de recambio.</p>\r\n\r\n<p><strong>d. &iquest;Cu&aacute;nto duran las bater&iacute;as?</strong></p>\r\n\r\n<p>El sistema funciona sin pilas, es completamente independiente de acuerdo con las condiciones de luz. Las c&eacute;lulas solares son las que proporcionan la energ&iacute;a necesaria para alimentar el visor, en condiciones de poca luz la recepci&oacute;n de energ&iacute;a falla y el visor se apaga.</p>\r\n\r\n<p><strong>e. &iquest;La eficiencia del visor permanece constante en el tiempo?</strong></p>\r\n\r\n<p>&iexcl;Por supuesto! Los primeros visores han operado durante m&aacute;s de cinco a&ntilde;os y no muestran ninguna disminuci&oacute;n en su rendimiento.</p>\r\n\r\n<p><strong>f. &iquest;Cu&aacute;les son los tiempos de respuesta, como se hace oscuro?</strong></p>\r\n\r\n<p>LC Drive reacciona instant&aacute;neamente a los cambios de brillo. El visor se adapta en pocas cent&eacute;simas de segundo.</p>\r\n\r\n<p><strong>g. &iquest;Y cuando el sol baja por el filtro de los &aacute;rboles y/o edificios molestos?</strong> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p>Tambi&eacute;n funciona en este caso. Ciertamente no se puede eliminar por completo el llamado efecto estrobosc&oacute;pico, pero continua siendo una ayuda valiosa.</p>\r\n\r\n<p><strong>h. &iquest;Qu&eacute; grosor tiene? &iquest;Cu&aacute;nto pesa?</strong></p>\r\n\r\n<p>El peso y el grosor son similares a los de cualquier visor Raleri antiempa&ntilde;ante. Es decir, pesa unas decenas de gramos y es de 0,5 mm de espesor. La junta adhesiva tiene 0,5 mm para un total de 1 mm en el borde aproximadamente.</p>\r\n\r\n<p><strong>i. &iquest;Necesita un mantenimiento especial?</strong></p>\r\n\r\n<p>LC Drive es un objeto delicado, pero como est&aacute; montado en una zona del casco naturalmente protegida, despu&eacute;s de la instalaci&oacute;n no necesita precauciones especiales.</p>', '<p>LC&nbsp;Drive&nbsp;hace la conducci&oacute;n m&aacute;s segura al resolver el problema de los cambios de brillo repentinos (ejemplo: por hileras de &aacute;rboles, largas carretera o entradas y salidas en t&uacute;neles), con tiempos de reacci&oacute;n casi inmediata. No es fabricado con la misma tecnolog&iacute;a de los visores PCSHADE FOGSTOP, pues utiliza un sistema de cristal l&iacute;quido que filtra m&aacute;s luz cuando se alimenta por c&eacute;lulas solares en una gama infinita. Adem&aacute;s, posee un excelente tratamiento antiempa&ntilde;ante.</p>\r\n\r\n<p><span style="line-height: 1.6em;">Dimensiones:&nbsp;275x115x2 mm.</span></p>\r\n\r\n<p>Para su instalaci&oacute;n, LC Drive utiliza una&nbsp;pared flexible que se adhiere a la visera del casco.&nbsp;El inserto se aplica por medio de un sello adhesivo no permanente, por lo que se proporciona un repuesto en la caja.</p>\r\n\r\n<p><span style="line-height: 1.6em;">En estos v&iacute;deos es posible conocer m&aacute;s sobre la instalaci&oacute;n y funcionamiento de este novedoso producto:</span></p>\r\n\r\n<p><a href="http://www.youtube.com/watch?v=XSvAX-uWhcI" style="line-height: 1.6em;">http://www.youtube.com/watch?v=XSvAX-uWhcI</a></p>\r\n\r\n<p><a href="http://www.youtube.com/watch?v=AKAD476NArE">http://www.youtube.com/watch?v=AKAD476NArE</a></p>', '31 LC DRIVE A Brown B.jpeg', '2014-03-02 20:35:22');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `categorias_productos`
-- 

CREATE TABLE `categorias_productos` (
  `id` int(2) NOT NULL auto_increment,
  `txt_nombre` varchar(200) NOT NULL,
  `txt_texto` text NOT NULL,
  `txt_caracteristicas` text NOT NULL,
  `imagen` varchar(500) NOT NULL,
  `num_precio` varchar(100) NOT NULL,
  `id_categoria` int(2) NOT NULL,
  `actualizado` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `categorias_productos`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `categorias_productos_imagenes`
-- 

CREATE TABLE `categorias_productos_imagenes` (
  `id` int(2) NOT NULL auto_increment,
  `imagen` varchar(500) NOT NULL,
  `id_producto` int(2) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Volcar la base de datos para la tabla `categorias_productos_imagenes`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `contenidos`
-- 

CREATE TABLE `contenidos` (
  `id` int(11) NOT NULL auto_increment,
  `txt_nombre` varchar(560) default NULL,
  `actualizado` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- Volcar la base de datos para la tabla `contenidos`
-- 

INSERT INTO `contenidos` VALUES (1, 'Quienes Somos', '2014-03-02 15:25:33');
INSERT INTO `contenidos` VALUES (2, 'Política', '2014-03-02 10:30:54');
INSERT INTO `contenidos` VALUES (3, 'Contacto', '2014-03-02 15:49:56');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `contenidos_campos`
-- 

CREATE TABLE `contenidos_campos` (
  `id` int(11) NOT NULL auto_increment,
  `id_contenido` int(11) NOT NULL,
  `id_tipo_campo` int(11) NOT NULL,
  `txt_titulo` varchar(450) NOT NULL,
  `txt_valor` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `id_tipo_campo` (`id_tipo_campo`),
  KEY `id_contenido` (`id_contenido`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

-- 
-- Volcar la base de datos para la tabla `contenidos_campos`
-- 

INSERT INTO `contenidos_campos` VALUES (1, 1, 6, 'Contenido', '<p>Raleri es una empresa ubicada en la ciudad de Bologna &ndash; Italia que con tecnolog&iacute;a de punta y materiales de &uacute;ltima generaci&oacute;n fabrica dispositivos de protecci&oacute;n visual con &eacute;nfasis en lentes y visores para cascos de motociclistas, as&iacute; como lentes de sol para la realizaci&oacute;n de diversas actividades.</p>\r\n\r\n<p>Adem&aacute;s de Europa, Raleri tiene presencia en Sudam&eacute;rica en pa&iacute;ses como Chile, Argentina y &nbsp;Ecuador. En el 2014 llega a Colombia a trav&eacute;s del sitio web <a href="http://www.Raleri.com.co">www.Raleri.com.co</a> donde es posible obtener diversos productos que satisfacen las necesidades de comodidad, seguridad e innovaci&oacute;n de motociclistas, deportistas y p&uacute;blico en general.</p>');
INSERT INTO `contenidos_campos` VALUES (2, 1, 2, 'Logo', 'logoquienes.png');
INSERT INTO `contenidos_campos` VALUES (3, 2, 6, 'Contenido', '<p>Todas las im&aacute;genes, videos y material informativo del presente sitio web fueron tomados de <a href="http://www.raleri.com">http://www.raleri.com</a>, <a href="http://www.youtube.com/user/raleri1">http://www.youtube.com/user/raleri1</a> y del perfil de Facebook <a href="https://es-es.facebook.com/raleri">RALERI s.r.l.</a> bajo autorizaci&oacute;n de Raleri para distribuir y comercializar &uacute;nicamente los productos de la marca en Colombia, quedando totalmente prohibida cualquier manipulaci&oacute;n indebida o plagio del material audiovisual contenido en estos sitios web sin aprobaci&oacute;n de sus autores</p>');
INSERT INTO `contenidos_campos` VALUES (4, 3, 1, 'Address', 'Cra 123 #14-34');
INSERT INTO `contenidos_campos` VALUES (5, 3, 1, 'Phone Number', '57(1) 123456');
INSERT INTO `contenidos_campos` VALUES (6, 3, 1, 'Cell phone number', '57(1) 123456');
INSERT INTO `contenidos_campos` VALUES (7, 3, 1, 'E-mail', 'info@raleri.com.co');
INSERT INTO `contenidos_campos` VALUES (8, 3, 2, 'Picture', 'logo_contacto.jpg');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `tipo_campo`
-- 

CREATE TABLE `tipo_campo` (
  `id` int(11) NOT NULL auto_increment,
  `txt_nombre` varchar(45) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- 
-- Volcar la base de datos para la tabla `tipo_campo`
-- 

INSERT INTO `tipo_campo` VALUES (1, 'text');
INSERT INTO `tipo_campo` VALUES (2, 'file');
INSERT INTO `tipo_campo` VALUES (3, 'hidden');
INSERT INTO `tipo_campo` VALUES (4, 'textarea');
INSERT INTO `tipo_campo` VALUES (5, 'document');
INSERT INTO `tipo_campo` VALUES (6, 'wysiwyg');
INSERT INTO `tipo_campo` VALUES (7, 'Gallery');

-- 
-- Filtros para las tablas descargadas (dump)
-- 

-- 
-- Filtros para la tabla `contenidos_campos`
-- 
ALTER TABLE `contenidos_campos`
  ADD CONSTRAINT `contenidos_campos_ibfk_1` FOREIGN KEY (`id_contenido`) REFERENCES `contenidos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `contenidos_campos_ibfk_2` FOREIGN KEY (`id_tipo_campo`) REFERENCES `tipo_campo` (`id`) ON UPDATE CASCADE;
