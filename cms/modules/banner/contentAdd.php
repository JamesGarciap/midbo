<?php
 include("moduleLinks.php"); 

 ?>

<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="<?php echo $mLinkDashboard ?>"><i class="icon-home"></i></a></li>
  <li><a href="<?php echo $mLinkModuleBanner ?>">Main Banner</a></li>
  <li><a href="javascript:void(0)">Add Banner</a></li>
  <li><span>Add Main Banner</span></li>
 </ul>
</div>

 <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                   
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Add Main Banner</h4>
                            </div>
                            <div class="w-box-content">
                              <form id="form" name="form" action="<?php echo $mLinkModuleActions ?>&action=add" method="post" enctype="multipart/form-data">

                                    <div class="formSep">
                                        <label>Logo</label>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 600px; height: 256px;">
                                             <img src="../img/dummy_1000x427" alt="" >
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="width: 600px; height: 256px;"></div>
                                            <span class="btn btn-small btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
                                            <input type="file" name="imagen"></span>
                                            <a href="#" class="btn btn-small btn-link fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>

                                    <div class="formSep">
                                        <label>Title</label>
                                        <input type="text" class="span8" id="s_name" name="titulo" value="" />
                                        <input type="hidden" class="span8" id="s_texto" name="texto" value="" />
                                    </div>
                                    
                                    <div class="formSep">
                                        <label>Link</label>
                                        <input type="text" class="span8" id="s_name" name="enlace" value="" />
                                    </div>
                                    
                                   
                                    <div class="formSep sepH_b">
                                        <input class="btn btn-beoro-3" type="submit" value="Save changes">
                                        <a href="<?php echo $mLinkModuleBanner ?>" class="btn btn-danger" id="" >Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php include("moduleScripts.php") ?>
