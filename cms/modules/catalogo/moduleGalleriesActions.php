<?php

  include("moduleLinks.php"); 

  $action = GetData( "action", FALSE );
  

  switch ($action) {

   case 'add':

     $location = "location: ".$mLinkModuleGalleries."&id=".GetData( 'id_producto', FALSE )."&categoria=".GetData( 'id_categoria', FALSE )."&executed=add";
     
     $cCategorias_productos_imagenes = new Dbcategorias_productos_imagenes();

     $cCategorias_productos_imagenes->settxt_nombre(GetData( 'txt_nombre', FALSE ));
     $cCategorias_productos_imagenes->setid_producto(GetData( 'id_producto', FALSE ));
     $cCategorias_productos_imagenes->setimagen(GetData( 'url', FALSE ));
     $cCategorias_productos_imagenes->settipo(GetData( 'tipo', FALSE ));

     $fileName1 = $_FILES['imagen']["name"];
    
     if ($fileName1 != ""){
         $destino = '../img/'.$fileName1;

         if (file_exists($destino)){
             //unlink($destino);
         }
         
         if(move_uploaded_file($_FILES['imagen'][ 'tmp_name' ], $destino)){
           $cCategorias_productos_imagenes->setimagen($fileName1);
         }
     }
          
     $cCategorias_productos_imagenes->save();

    break;


   case 'delete':

     $location = "location: ".$mLinkModuleGalleries."&id=".GetData( 'producto', FALSE )."&categoria=".GetData( 'categoria', FALSE )."&executed=delete";
     
     $mId = GetData( "id", FALSE );
     
     $cCategorias_productos_imagenes = new Dbcategorias_productos_imagenes();
     $cCategorias_productos_imagenes->deleteById($mId);

    break;
   
  

   case 'edit':

     $location = "location: ".$mLinkModuleGalleries."&id=".GetData( 'id_producto', FALSE )."&categoria=".GetData( 'id_categoria', FALSE )."&executed=edit";
 
     $mId = GetData( "id", FALSE );
     $mIdTipo = GetData( "tipo", FALSE );

     $cCategorias_productos_imagenes = new Dbcategorias_productos_imagenes();
     $cCategorias_productos_imagenes->setid($mId);
     $cCategorias_productos_imagenes->settxt_nombre(GetData( 'txt_nombre', FALSE ));
     $cCategorias_productos_imagenes->setid_producto(GetData( 'id_producto', FALSE ));
     
     if ($mIdTipo == 2) {
       $cCategorias_productos_imagenes->setimagen(GetData( 'url', FALSE ));
     }

     $fileName1 = $_FILES['imagen']["name"];
    
     if ($fileName1 != ""){
         $destino = '../img/'.$fileName1;

         if (file_exists($destino)){
             //unlink($destino);
         }
         
         if(move_uploaded_file($_FILES['imagen'][ 'tmp_name' ], $destino)){
           $cCategorias_productos_imagenes->setimagen($fileName1);
         }
     }
          
     $cCategorias_productos_imagenes->save();
    break;

   
  }



header($location);
exit;



 ?>