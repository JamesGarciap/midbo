<?php
 include("moduleLinks.php"); 

  $mId = GetData( "id", FALSE );
  $mIdProducto = GetData( "producto", FALSE );
  $mIdCategoria = GetData( "categoria", FALSE );

  $cCategorias = new Dbcategorias();
  $mCategoria = $cCategorias->getByPk($mIdCategoria);

  $cCategorias_productos = new Dbcategorias_productos();
  $mProducto = $cCategorias_productos->getByPk($mIdProducto);

  $cCategorias_productos_imagenes = new Dbcategorias_productos_imagenes();
  $mData = $cCategorias_productos_imagenes->getByPk($mId);

 ?>

<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="<?php echo $mLinkDashboard ?>"><i class="icon-home"></i></a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Catalog</a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Categories</a></li>
  <li><a href="<?php echo $mLinkModuleProducts ?>&id=<?php echo $mIdCategoria ?>"><?php echo $mCategoria['txt_nombre'] ?></a></li> 
  <li><a href="<?php echo $mLinkModuleGalleries ?>&id=<?php echo $mIdProducto ?>&categoria=<?php echo $mIdCategoria ?>"><?php echo $mProducto['txt_nombre'] ?></a></li>
  <li><a href="javascript:void(0)">Edit Resource</a></li>
 </ul>
</div>

 <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                   
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Edit Resource</h4>
                            </div>
                            <div class="w-box-content">
                              <form id="form" name="form" action="<?php echo $mLinkModuleGalleriesActions ?>&action=edit" method="post" enctype="multipart/form-data">
                                  
                                    <input type="hidden" name="id" value="<?php echo $mData['id'] ?>">
                                    <input type="hidden" name="id_producto" value="<?php echo $mIdProducto ?>">
                                    <input type="hidden" name="id_categoria" value="<?php echo $mIdCategoria ?>">
                                    <input type="hidden" name="tipo" value="<?php echo $mData['tipo'] ?>">

                                    <div class="formSep">
                                        <label>Resource Name</label>
                                        <input type="text" class="span8" id="s_name" name="txt_nombre" value="<?php echo $mData['txt_nombre'] ?>" />
                                    </div>

                                    <?php if ($mData['tipo'] == 1): ?>
                                        

                                        <div class="formSep">
                                            <label>Image</label>
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail" style="width: 272px; height: 136px;">
                                                   <img src="../img/<?php echo $mData['imagen'] ?>" alt="" >
                                                </div>
                                                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 272px; height: 136px;"></div>
                                                <span class="btn btn-small btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
                                                <input type="file" name="imagen"></span>
                                                <a href="#" class="btn btn-small btn-link fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>

                                    <?php else: ?>
                                        
                                        <div class="formSep">
                                            <label>Iframe URL</label>
                                            <input type="text" class="span8" id="s_url" name="url" value="<?php echo $mData['imagen'] ?>" />
                                        </div>

                                    <?php endif ?>
                                   
                                    <div class="formSep sepH_b">
                                        <input class="btn btn-beoro-3" type="submit" value="Save changes">
                                        <a href="<?php echo $mLinkModuleGalleries ?>&id=<?php echo $mIdProducto ?>&categoria=<?php echo $mIdCategoria ?>" class="btn btn-danger" id="" >Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php include("moduleScripts.php") ?>
