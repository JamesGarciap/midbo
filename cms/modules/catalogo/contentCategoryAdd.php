<?php
 include("moduleLinks.php"); 

 ?>

<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="<?php echo $mLinkDashboard ?>"><i class="icon-home"></i></a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Catalog</a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Categories</a></li>
  <li><a href="javascript:void(0)">Add Category</a></li>
 </ul>
</div>

 <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                   
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Add Category</h4>
                            </div>
                            <div class="w-box-content">
                              <form id="form" name="form" action="<?php echo $mLinkModuleCategoryActions ?>&action=add" method="post" enctype="multipart/form-data">

                                    <div class="formSep">
                                        <label>Category</label>
                                        <input type="text" class="span8" id="s_name" name="txt_nombre" value="" />
                                    </div>

                                   
                                    <div class="formSep sepH_b">
                                        <input class="btn btn-beoro-3" type="submit" value="Save changes">
                                        <a href="<?php echo $mLinkModuleCatalogo ?>" class="btn btn-danger" id="" >Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php include("moduleScripts.php") ?>
