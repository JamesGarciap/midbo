<?php

  include("moduleLinks.php"); 

  $action = GetData( "action", FALSE );
  

  switch ($action) {

   case 'add':

     $location = "location: ".$mLinkModuleCatalogo."&executed=add";
     
     $cCategorias = new Dbcategorias();

     $cCategorias->setactualizado(date("Y-m-d H:i:s"));
     $cCategorias->settxt_nombre(GetData( 'txt_nombre', FALSE ));

     $cCategorias->save();

    break;

   case 'delete':

     $location = "location: ".$mLinkModuleCatalogo."&executed=delete";
     
     $mId = GetData( "id", FALSE );
     
     $cCategorias = new Dbcategorias();
     $cCategorias->deleteById($mId);

    break;
   
  

   case 'edit':

     $location = "location: ".$mLinkModuleCatalogo."&executed=edit";
 
     $mId = GetData( "id", FALSE );
     $cCategorias = new Dbcategorias();
     $cCategorias->setid($mId);
     $cCategorias->setactualizado(date("Y-m-d H:i:s"));
     $cCategorias->settxt_nombre(GetData( 'txt_nombre', FALSE ));

          
     $cCategorias->save();
    break;
   
  }



header($location);
exit;



 ?>