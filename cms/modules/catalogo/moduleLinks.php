<?php 

    
    $mLinkModuleCategoryEdit = Link::ToSection( array( "s" => "catalogo", "ss" => "contentCategoryEdit" ) );
    $mLinkModuleCategoryAdd = Link::ToSection( array( "s" => "catalogo", "ss" => "contentCategoryAdd" ) );
    $mLinkModuleCategoryActions = Link::ToSection( array( "s" => "catalogo", "ss" => "moduleCategoryActions" ) );
        

    $mLinkModuleProducts = Link::ToSection( array( "s" => "catalogo", "ss" => "overviewProducts" ) );
    $mLinkModuleProductsAdd = Link::ToSection( array( "s" => "catalogo", "ss" => "contentProductsAdd" ) );
    $mLinkModuleProductsEdit = Link::ToSection( array( "s" => "catalogo", "ss" => "contentProductsEdit" ) );
    $mLinkModuleProductsActions = Link::ToSection( array( "s" => "catalogo", "ss" => "moduleProductActions" ) );


    $mLinkModuleGalleries = Link::ToSection( array( "s" => "catalogo", "ss" => "overviewGalleries" ) );
    $mLinkModuleGalleriesAdd = Link::ToSection( array( "s" => "catalogo", "ss" => "contentGalleriesAdd" ) );
    $mLinkModuleGalleriesEdit = Link::ToSection( array( "s" => "catalogo", "ss" => "contentGalleriesEdit" ) );
    $mLinkModuleGalleriesActions = Link::ToSection( array( "s" => "catalogo", "ss" => "moduleGalleriesActions" ) );


 ?>
<!-- datatables -->
<link rel="stylesheet" href="js/lib/datatables/css/datatables_beoro.css">
<!-- sticky notifications -->
<link rel="stylesheet" href="js/lib/sticky/sticky.css">


<script type="text/javascript">

      function confirma(formObj) { 
        if(!confirm("¿Really want to run this action?")) { 
              return false;
         }
      }

</script>
