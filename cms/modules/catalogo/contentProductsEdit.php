<?php
 include("moduleLinks.php"); 

  $mIdCategoria = GetData( "categoria", FALSE );
  $cCategorias = new Dbcategorias();
  $mCategoria = $cCategorias->getByPk($mIdCategoria);


  $mId = GetData( "id", FALSE );
  $cCategorias_productos = new Dbcategorias_productos();
  $mData = $cCategorias_productos->getByPk($mId);



 ?>

<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="<?php echo $mLinkDashboard ?>"><i class="icon-home"></i></a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Catalog</a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Categories</a></li>
  <li><a href="<?php echo $mLinkModuleProducts ?>&id=<?php echo $mIdCategoria ?>"><?php echo $mCategoria['txt_nombre'] ?></a></li>
  <li><a href="javascript:void(0)">Edit Product</a></li>
  <li><a href="javascript:void(0)"><?php echo $mData['txt_nombre'] ?></a></li>
 </ul>
</div>

 <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                   
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Edit Main Banner</h4>
                            </div>
                            <div class="w-box-content">
                              <form id="form" name="form" action="<?php echo $mLinkModuleProductsActions ?>&action=edit" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?php echo $mData['id'] ?>">
                                <input type="hidden" name="id_categoria" value="<?php echo $mIdCategoria ?>">

                                <div class="formSep">

                                    <label>Image</label>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 580x; height: 211px;">
                                             <img src="../img/productos/<?php echo $mData['imagen'] ?>" alt="" >
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="width: 580x; height: 211px;"></div>
                                            <span class="btn btn-small btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
                                            <input type="file" name="imagen"></span>
                                            <a href="#" class="btn btn-small btn-link fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                                    <label>Render</label>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 710px; height: 425px;">
                                             <img src="../img/render/<?php echo $mData['render'] ?>" alt="" >
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="width: 710px; height: 425px;"></div>
                                            <span class="btn btn-small btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
                                            <input type="file" name="render"></span>
                                            <a href="#" class="btn btn-small btn-link fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>

                                    <div class="formSep">
                                        <label>Product Name</label>
                                        <input type="text" class="span8" id="s_name" name="txt_nombre" value="<?php echo $mData['txt_nombre'] ?>" />
                                    </div>

                                    <div class="formSep">
                                        <label>Short Description</label>
                                        <textarea id="wysiwg_editor" cols="30" rows="10" name="txt_texto"><?php echo $mData['txt_texto'] ?></textarea>
                                    </div>
                                    
                                    <div class="formSep">
                                        <label>Description</label>
                                        <textarea id="wysiwg_editor2" cols="30" rows="10" name="txt_caracteristicas"><?php echo $mData['txt_caracteristicas'] ?></textarea>
                                    </div>
                                    
                                    <div class="formSep">
                                        <label>Features</label>
                                        <input type="file" class="span3" id="s_price" name="archivo" value="" />
                                    </div>

                                   
                                    <div class="formSep sepH_b">
                                        <input class="btn btn-beoro-3" type="submit" value="Save changes">
                                        <a href="<?php echo $mLinkModuleProducts ?>&id=<?php echo $mIdCategoria ?>" class="btn btn-danger" id="" >Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php include("moduleScripts.php") ?>
