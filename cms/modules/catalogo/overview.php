<?php
 include("moduleLinks.php"); 

 $cCategorias = new Dbcategorias();
 $mData = $cCategorias->getList();
 $mAtion = GetData( "executed", FALSE );

 switch ($mAtion) {

     case 'delete':
         $mSuccess = "error";
         $mSuccessText = "delete";
         break;

     case 'edit':
         $mSuccess = "success";
         $mSuccessText = "edit";
         break;
     
     default:
         $mSuccess = "success";
         $mSuccessText = "add";

         break;
 }

 ?>
<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="<?php echo $mLinkDashboard ?>"><i class="icon-home"></i></a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Catalog</a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Categories</a></li>
 </ul>
</div>

<div class="container">

<?php if ($mAtion): ?>
    
    <div class="alert alert-<?php echo $mSuccess ?>">
        <a data-dismiss="alert" class="close">×</a>
        <strong>Success!</strong> Successful <?php echo $mSuccessText ?> content.
    </div>

<?php endif ?>
</div>

<!-- main content -->
<div class="container">
    <div class="row-fluid">
        <div class="span12">

            <div class="w-box">
                <div class="w-box-header">
                    <div class="btn-group">
                        <a href="<?php echo $mLinkModuleCategoryAdd ?>" class="btn btn-primary btn-mini delete_rows_simple" data-tableid="smpl_tbl" title="Add Banner">Add Category</a>
                    </div>
                </div>
                <div class="w-box-content">
                    <table class="table table-vam table-striped" id="dt_gal">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                         <?php if ($mData): ?>
                          <?php foreach ($mData as $content): ?>
                           
                          
                            <tr>
                                <td><?php echo utf8_encode($content['txt_nombre']) ?></td>
                                <td><?php echo $content['actualizado'] ?></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="<?php echo $mLinkModuleCategoryEdit ?>&id=<?php echo $content['id'] ?>" class="btn btn-mini" title="Edit"><i class="icon-pencil"></i></a>
                                        <a href="<?php echo $mLinkModuleProducts ?>&id=<?php echo $content['id'] ?>" class="btn btn-mini" title="Products"><i class="icsw16-tags-2"></i></a>
                                        <a href="<?php echo $mLinkModuleCategoryActions ?>&action=delete&id=<?php echo $content['id'] ?>" class="btn btn-mini" title="Delete"><i class="icon-trash" onclick="return confirma(this)"></i></a>
                                    </div>
                                </td>
                            </tr>
                           
                          <?php endforeach ?>
                         <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- confirmation box -->
            <div class="hide">
                <div id="confirm_dialog" class="cbox_content">
                    <div class="sepH_c"><strong>Are you sure you want to delete this row(s)?</strong></div>
                    <div>
                        <a href="#" class="btn btn-small btn-beoro-3 confirm_yes">Yes</a>
                        <a href="#" class="btn btn-small confirm_no">No</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include("moduleScripts.php") ?>