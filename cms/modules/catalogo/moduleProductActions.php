<?php

  include("moduleLinks.php"); 

  $action = GetData( "action", FALSE );
  

  switch ($action) {

   case 'add':

     $location = "location: ".$mLinkModuleProducts."&id=".GetData( 'id_categoria', FALSE )."&executed=add";
     
     $cCategorias_productos = new Dbcategorias_productos();

     $cCategorias_productos->setactualizado(date("Y-m-d H:i:s"));
     $cCategorias_productos->settxt_nombre(GetData( 'txt_nombre', FALSE ));
     $cCategorias_productos->settxt_texto(GetData( 'txt_texto', FALSE ));
     $cCategorias_productos->settxt_caracteristicas(GetData( 'txt_caracteristicas', FALSE ));
     $cCategorias_productos->setid_categoria(GetData( 'id_categoria', FALSE ));

     $fileName1 = $_FILES['imagen']["name"];
    
     if ($fileName1 != ""){
         $destino = '../img/productos/'.$fileName1;

         if (file_exists($destino)){
             //unlink($destino);
         }
         
         if(move_uploaded_file($_FILES['imagen'][ 'tmp_name' ], $destino)){
           $cCategorias_productos->setimagen($fileName1);
         }
     }
          
     $fileName2 = $_FILES['render']["name"];
    
     if ($fileName2 != ""){
         $fileName2 = "render_".$fileName2;
         $destino = '../img/render/'.$fileName2;

         if (file_exists($destino)){
             //unlink($destino);
         }
         
         if(move_uploaded_file($_FILES['render'][ 'tmp_name' ], $destino)){
           $cCategorias_productos->setrender($fileName2);
         }
     }
          
     $fileName3 = $_FILES['archivo']["name"];
    
     if ($fileName3 != ""){
         $fileName3 = "file_".$fileName3;
         $destino = '../files/'.$fileName3;

         if (file_exists($destino)){
             //unlink($destino);
         }
         
         if(move_uploaded_file($_FILES['archivo'][ 'tmp_name' ], $destino)){
           $cCategorias_productos->setarchivo($fileName3);
         }
     }
          
     $cCategorias_productos->save();

    break;

   case 'delete':

     $location = "location: ".$mLinkModuleProducts."&id=".GetData( 'categoria', FALSE )."&executed=delete";
     
     $mId = GetData( "id", FALSE );
     
     $cCategorias_productos = new Dbcategorias_productos();
     $cCategorias_productos->deleteById($mId);

    break;
   
  

   case 'edit':

     $location = "location: ".$mLinkModuleProducts."&id=".GetData( 'id_categoria', FALSE )."&executed=edit";
 
     $mId = GetData( "id", FALSE );
     $cCategorias_productos = new Dbcategorias_productos();
     $cCategorias_productos->setid($mId);
     $cCategorias_productos->setactualizado(date("Y-m-d H:i:s"));
     $cCategorias_productos->settxt_nombre(GetData( 'txt_nombre', FALSE ));
     $cCategorias_productos->settxt_texto(GetData( 'txt_texto', FALSE ));
     $cCategorias_productos->settxt_caracteristicas(GetData( 'txt_caracteristicas', FALSE ));

     $fileName1 = $_FILES['imagen']["name"];
    
     if ($fileName1 != ""){
         $destino = '../img/productos/'.$fileName1;

         if (file_exists($destino)){
             //unlink($destino);
         }
         
         if(move_uploaded_file($_FILES['imagen'][ 'tmp_name' ], $destino)){
           $cCategorias_productos->setimagen($fileName1);
         }
     }
          
     $fileName2 = $_FILES['render']["name"];
    
     if ($fileName2 != ""){
         $fileName2 = "render_".$fileName2;
         $destino = '../img/render/'.$fileName2;

         if (file_exists($destino)){
             //unlink($destino);
         }
         
         if(move_uploaded_file($_FILES['render'][ 'tmp_name' ], $destino)){
           $cCategorias_productos->setrender($fileName2);
         }
     }
          
     $fileName3 = $_FILES['archivo']["name"];
    
     if ($fileName3 != ""){
         $fileName3 = "file_".$fileName3;
         $destino = '../files/'.$fileName3;

         if (file_exists($destino)){
             //unlink($destino);
         }
         
         if(move_uploaded_file($_FILES['archivo'][ 'tmp_name' ], $destino)){
           $cCategorias_productos->setarchivo($fileName3);
         }
     }
          
     $cCategorias_productos->save();
    break;

   case 'favorite':

     $location = "location: ".$mLinkModuleProducts."&id=".GetData( 'categoria', FALSE )."&executed=favorite";
 
     $mId = GetData( "id", FALSE );
     $mDestacado = GetData( 'destacado', FALSE );
     $cCategorias_productos = new Dbcategorias_productos();
     $cCategorias_productos->setid($mId);
     $cCategorias_productos->setactualizado(date("Y-m-d H:i:s"));
     
     if ($mDestacado == 1) {
        $cCategorias_productos2 = new Dbcategorias_productos();
        $mProductosDestacados = $cCategorias_productos2->getList( array( "destacado" => 1) );

          $cCategorias_productos->setdestacado($mDestacado);
        
     }
     else{
       $cCategorias_productos->setdestacado($mDestacado);        
     }
     
     $cCategorias_productos->save();
     break;
   
  }



header($location);
exit;



 ?>