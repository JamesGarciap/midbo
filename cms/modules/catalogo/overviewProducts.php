<?php
 include("moduleLinks.php"); 

  $mId = GetData( "id", FALSE );
  $cCategorias = new Dbcategorias();
  $mCategoria = $cCategorias->getByPk($mId);

  $cCategorias_productos = new Dbcategorias_productos();
  $mData = $cCategorias_productos->getList( array( "id_categoria" => $mCategoria["id"] ) );

  $mAtion = GetData( "executed", FALSE );


  switch ($mAtion) {

     case 'delete':
         $mSuccess = "error";
         $mSuccessText = "delete";
         break;

     case 'edit':
         $mSuccess = "success";
         $mSuccessText = "edit";
         break;
     
     case 'favorite':
         $mSuccess = "success";
         $mSuccessText = "highlight";
         break;

     case 'favoriteError':
         $mSuccess = "error";
         $mSuccessText = "favorite";
         break;
     
     default:
         $mSuccess = "success";
         $mSuccessText = "add";

         break;
  }

 ?>
<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="<?php echo $mLinkDashboard ?>"><i class="icon-home"></i></a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Catalog</a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Categories</a></li>
  <li><a href="#"><?php echo $mCategoria['txt_nombre'] ?></a></li>
 </ul>
</div>

<div class="container">

<?php if ($mAtion): ?>
    
    <div class="alert alert-<?php echo $mSuccess ?>">
        <a data-dismiss="alert" class="close">×</a>

        <?php if ($mAtion == "favoriteError"): ?>
        
          <strong>Success!</strong> You can't have more than 3 outstanding products.
        <?php else: ?>
          <strong>Success!</strong> Successful <?php echo $mSuccessText ?> content.
        
        <?php endif ?>

    </div>

<?php endif ?>
</div>

<!-- main content -->
<div class="container">
    <div class="row-fluid">
        <div class="span12">

            <div class="w-box">
                <div class="w-box-header">
                    <div class="btn-group">
                        <a href="<?php echo $mLinkModuleProductsAdd ?>&categoria=<?php echo $mId ?>" class="btn btn-primary btn-mini delete_rows_simple" data-tableid="smpl_tbl" title="Add Banner">Add Product</a>
                    </div>
                </div>
                <div class="w-box-content">
                    <table class="table table-vam table-striped" id="dt_gal">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                         <?php if ($mData): ?>
                          <?php foreach ($mData as $content): ?>
                           
                          
                            <tr>
                                <td style="width:100px">
                                    <a href="../img/productos/<?php echo $content['imagen'] ?>" title="Image 02" class="thumbnail">
                                        <img alt="" src="../img/productos/<?php echo $content['imagen'] ?>" style="width:80px">
                                    </a>
                                </td>
                                <td><?php echo utf8_encode($content['txt_nombre']) ?></td>
                                <td><?php echo $content['actualizado'] ?></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="<?php echo $mLinkModuleProductsEdit ?>&id=<?php echo $content['id'] ?>&categoria=<?php echo $mId ?>" class="btn btn-mini" title="Edit"><i class="icon-pencil"></i></a>
                                        <a href="<?php echo $mLinkModuleGalleries ?>&id=<?php echo $content['id'] ?>&categoria=<?php echo $mId ?>" class="btn btn-mini" title="Gallery"><i class="icsw16-image2-2"></i></a>
                                                                   
                                        <a href="<?php echo $mLinkModuleProductsActions ?>&action=delete&id=<?php echo $content['id'] ?>&categoria=<?php echo $mId ?>" class="btn btn-mini" title="Delete" onclick="return confirma(this)"><i class="icon-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                           
                          <?php endforeach ?>
                         <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- confirmation box -->

        </div>
    </div>
</div>


<?php include("moduleScripts.php") ?>