<?php
 include("moduleLinks.php"); 

  $mId = GetData( "id", FALSE );
  $mIdCategoria = GetData( "categoria", FALSE );
  $mIdTipo = GetData( "tipo", FALSE );

  $cCategorias = new Dbcategorias();
  $mCategoria = $cCategorias->getByPk($mIdCategoria);

  $cCategorias_productos = new Dbcategorias_productos();
  $mProducto = $cCategorias_productos->getByPk($mId);

 ?>

<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="<?php echo $mLinkDashboard ?>"><i class="icon-home"></i></a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Catalog</a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Categories</a></li>
  <li><a href="<?php echo $mLinkModuleProducts ?>&id=<?php echo $mIdCategoria ?>"><?php echo $mCategoria['txt_nombre'] ?></a></li> 
  <li><a href="<?php echo $mLinkModuleGalleries ?>&id=<?php echo $mId ?>&categoria=<?php echo $mIdCategoria ?>"><?php echo $mProducto['txt_nombre'] ?></a></li>
  <li><a href="javascript:void(0)">Add Resource</a></li>
 </ul>
</div>

 <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                   
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Add Resource</h4>
                            </div>
                            <div class="w-box-content">
                              <form id="form" name="form" action="<?php echo $mLinkModuleGalleriesActions ?>&action=add" method="post" enctype="multipart/form-data">
                                  
                                    <input type="hidden" name="id_producto" value="<?php echo $mId ?>">
                                    <input type="hidden" name="id_categoria" value="<?php echo $mIdCategoria ?>">
                                    <input type="hidden" name="tipo" value="<?php echo $mIdTipo ?>">

                                    <div class="formSep">
                                        <label>Resource Name</label>
                                        <input type="text" class="span8" id="s_name" name="txt_nombre" value="" />
                                    </div>

                                    <?php if ($mIdTipo == 1): ?>
                                        

                                        <div class="formSep">
                                            <label>Image</label>
                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <div class="fileupload-new thumbnail" style="width: 469px; height: 200px;">
                                                 <img src="../img/dummy_469x200" alt="" >
                                                </div>
                                                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 469px; height: 200px;"></div>
                                                <span class="btn btn-small btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
                                                <input type="file" name="imagen"></span>
                                                <a href="#" class="btn btn-small btn-link fileupload-exists" data-dismiss="fileupload">Remove</a>
                                            </div>
                                        </div>

                                    <?php else: ?>
                                        
                                        <div class="formSep">
                                            <label>Iframe URL</label>
                                            <input type="text" class="span8" id="s_url" name="url" value="" />
                                        </div>

                                    <?php endif ?>
                                   
                                    <div class="formSep sepH_b">
                                        <input class="btn btn-beoro-3" type="submit" value="Save changes">
                                        <a href="<?php echo $mLinkModuleGalleries ?>&id=<?php echo $mId ?>&categoria=<?php echo $mIdCategoria ?>" class="btn btn-danger" id="" >Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php include("moduleScripts.php") ?>
