
        <!-- datatables -->
            <script src="js/lib/datatables/js/jquery.dataTables.min.js"></script>
            <script src="js/lib/datatables/js/jquery.dataTables.sorting.js"></script>
        <!-- datatables bootstrap integration -->
            <script src="js/lib/datatables/js/jquery.dataTables.bootstrap.min.js"></script>
            
        <!-- colorbox -->
            <script src="js/lib/colorbox/jquery.colorbox.min.js"></script>
        <!-- responsive image grid -->
            <script src="js/lib/wookmark/jquery.imagesloaded.min.js"></script>
            <script src="js/lib/wookmark/jquery.wookmark.min.js"></script>

            <script src="js/pages/beoro_gallery.js"></script>
            
            <script src="js/pages/beoro_tables.js"></script>

            
    <!-- Forms -->  
        <!-- jQuery UI -->
            <script src="js/lib/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
        <!-- touch event support for jQuery UI -->
            <script src="js/lib/jquery-ui/jquery.ui.touch-punch.min.js"></script>
        <!-- progressbar animations -->
            <script src="js/form/jquery.progressbar.anim.min.js"></script>
        <!-- 2col multiselect -->
            <script src="js/lib/multi-select/js/jquery.multi-select.min.js"></script>
            <script src="js/lib/multi-select/js/jquery.quicksearch.min.js"></script>
        <!-- combobox -->
            <script src="js/form/fuelux.combobox.min.js"></script>
        <!-- file upload widget -->
            <script src="js/form/bootstrap-fileupload.min.js"></script>
        <!-- masked inputs -->
            <script src="js/lib/jquery-inputmask/jquery.inputmask.min.js"></script>
            <script src="js/lib/jquery-inputmask/jquery.inputmask.extensions.js"></script>
            <script src="js/lib/jquery-inputmask/jquery.inputmask.date.extensions.js"></script>
        <!-- enchanced select box, tag handler -->
            <script src="js/lib/select2/select2.min.js"></script>
        <!-- password strength metter -->
            <script src="js/lib/pwdMeter/jquery.pwdMeter.min.js"></script>
        <!-- datepicker -->
            <script src="js/lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        <!-- timepicker -->
            <script src="js/lib/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
        <!-- colorpicker -->
            <script src="js/lib/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
        <!-- metadata -->
            <script src="js/lib/ibutton/js/jquery.metadata.min.js"></script>
        <!-- switch buttons -->
            <script src="js/lib/ibutton/js/jquery.ibutton.beoro.js"></script>
        <!-- autosize textarea -->
            <script src="js/form/jquery.autosize.min.js"></script>
        <!-- textarea counter -->
            <script src="js/lib/jquery-textarea-counter/jquery.textareaCounter.plugin.min.js"></script>
        <!-- UI Spinners -->
            <script src="js/lib/jqamp-ui-spinner/globalize/globalize.min.js"></script>
            <script src="js/lib/jqamp-ui-spinner/globalize/cultures/globalize.culture.fr-FR.js"></script>
            <script src="js/lib/jqamp-ui-spinner/globalize/cultures/globalize.culture.ja-JP.js"></script>
            <script src="js/lib/jqamp-ui-spinner/globalize/cultures/globalize.culture.zh-CN.js"></script>
            <script src="js/lib/jqamp-ui-spinner/compiled/jqamp-ui-spinner.min.js"></script>
            <script src="js/lib/jqamp-ui-spinner/compiled/jquery-mousewheel-3.0.6.min.js"></script>
        <!-- plupload and the jQuery queue widget -->
            <script type="text/javascript" src="js/lib/plupload/js/plupload.full.js"></script>
            <script type="text/javascript" src="js/lib/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
        <!-- WYSIWG Editor -->
            <script src="js/lib/ckeditor/ckeditor.js"></script>

            <script src="js/pages/beoro_form_elements.js"></script>

            
