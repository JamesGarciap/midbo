<?php
 include("moduleLinks.php"); 

  $mId = GetData( "id", FALSE );
  $mIdCategoria = GetData( "categoria", FALSE );

  $cCategorias = new Dbcategorias();
  $mCategoria = $cCategorias->getByPk($mIdCategoria);

  $cCategorias_productos = new Dbcategorias_productos();
  $mProducto = $cCategorias_productos->getByPk($mId);

  $cCategorias_productos_imagenes = new Dbcategorias_productos_imagenes();
  $mData = $cCategorias_productos_imagenes->getList( array( "id_producto" => $mId ) );

  $mAtion = GetData( "executed", FALSE );


    function video_image($url){
        $image_url = parse_url($url);
        $image_url = str_replace("//", "", $image_url);
        if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
            $array = explode("&", $image_url['query']);
            return "http://img.youtube.com/vi/".substr($array[0], 2)."/0.jpg";
        } else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
            $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
            return $hash[0]["thumbnail_large"];
        }
    }

  switch ($mAtion) {

     case 'delete':
         $mSuccess = "error";
         $mSuccessText = "delete";
         break;

     case 'edit':
         $mSuccess = "success";
         $mSuccessText = "edit";
         break;

     
     default:
         $mSuccess = "success";
         $mSuccessText = "add";

         break;
  }

 ?>
<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="<?php echo $mLinkDashboard ?>"><i class="icon-home"></i></a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Catalog</a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Categories</a></li>
  <li><a href="<?php echo $mLinkModuleProducts ?>&id=<?php echo $mIdCategoria ?>"><?php echo $mCategoria['txt_nombre'] ?></a></li> 
  <li><a href="#"><?php echo $mProducto['txt_nombre'] ?></a></li>
 </ul>
</div>

<div class="container">

<?php if ($mAtion): ?>
    
    <div class="alert alert-<?php echo $mSuccess ?>">
        <a data-dismiss="alert" class="close">×</a>


          <strong>Success!</strong> Successful <?php echo $mSuccessText ?> content.
        

    </div>

<?php endif ?>
</div>

<!-- main content -->
<div class="container">
    <div class="row-fluid">
        <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Gallery</h4>
                                <div class="pull-right">
                                    <div class="toggle-group">
                                        <span class="dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span></span>
                                        <ul class="dropdown-menu">

                                            <li><a href="<?php echo $mLinkModuleGalleriesAdd ?>&id=<?php echo $mId ?>&categoria=<?php echo $mIdCategoria ?>&tipo=1" class="">Add Image</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="w-box-content cnt_a">

                                <div id="large_grid" class="wmk_grid">
                                    <ul>
                                    <?php if ($mData): ?>
                                      <?php foreach ($mData as $content): ?>
                                        

                                        <li id="<?php echo $content['id'] ?>" class="folder_all folder_2" data-folder="folder_2">
                                            <span class="img_holder">

                                              <?php if ($content['tipo']==1): 
                                                $content['imagen'] = "../img/".$content['imagen'];
                                              ?>
                                                <img src="<?php echo $content['imagen'] ?>" alt="" />
                                              
                                              <?php else: 
                                                $arrayVideo = explode("/", $content['imagen']);
                                                $content['imagen'] = video_image('https://www.youtube.com/watch?v='.end($arrayVideo));
                                              ?>
                                              
                                                <img src="<?php echo $content['imagen'] ?>" alt="" />

                                              <?php endif ?>
                                              
                                                <span class="imgTitle"><?php echo $content['txt_nombre'] ?></span>
                                            </span>
                                            <p class="img_actions">
                                                <a class="img_action_zoom" href="<?php echo $content['imagen'] ?>" title="Lorem ipsum dolor..."><i class="icon-search"></i></a>
                                                <a class="" href="<?php echo $mLinkModuleGalleriesEdit ?>&id=<?php echo $content['id'] ?>&producto=<?php echo $mId ?>&categoria=<?php echo $mIdCategoria ?>" title="Edit"><i class="icon-pencil"></i></a>
                                                <a class="" href="<?php echo $mLinkModuleGalleriesActions ?>&action=delete&id=<?php echo $content['id'] ?>&producto=<?php echo $mId ?>&categoria=<?php echo $mIdCategoria ?>" title="Remove"  onclick="return confirma(this)"><i class="icon-trash"></i></a>
                                            </p>
                                        </li>
                                      <?php endforeach ?>

                                    <?php endif ?>
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
            <!-- confirmation box -->
            <div class="hide">
                <div id="confirm_dialog" class="cbox_content">
                    <div class="sepH_c"><strong>Are you sure you want to delete this row(s)?</strong></div>
                    <div>
                        <a href="#" class="btn btn-small btn-beoro-3 confirm_yes">Yes</a>
                        <a href="#" class="btn btn-small confirm_no">No</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include("moduleScripts.php") ?>