<?php
 include("moduleLinks.php"); 

  $mId = GetData( "id", FALSE );
  $cCategorias = new Dbcategorias();
  $mData = $cCategorias->getByPk($mId);

 ?>

<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="<?php echo $mLinkDashboard ?>"><i class="icon-home"></i></a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Catalog</a></li>
  <li><a href="<?php echo $mLinkModuleCatalogo ?>">Categories</a></li>
  <li><a href="javascript:void(0)">Edit Category</a></li>
  <li><span><?php echo utf8_encode($mData['txt_nombre']) ?></span></li>
 </ul>
</div>

 <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                   
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Edit Main Banner</h4>
                            </div>
                            <div class="w-box-content">
                              <form id="form" name="form" action="<?php echo $mLinkModuleCategoryActions ?>&action=edit" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?php echo $mData['id'] ?>">
                                <div class="formSep">

                                    <div class="formSep">
                                        <label>Title</label>
                                        <input type="text" class="span8" id="s_name" name="txt_nombre" value="<?php echo $mData['txt_nombre'] ?>" />
                                    </div>

                             
                                    <div class="formSep sepH_b">
                                        <input class="btn btn-beoro-3" type="submit" value="Save changes">
                                        <a href="<?php echo $mLinkModuleCatalogo ?>" class="btn btn-danger" id="" >Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php include("moduleScripts.php") ?>
