<?php

  include("moduleLinks.php"); 

  $action = GetData( "action", FALSE );
  

  switch ($action) {

   case 'edit':

     $location = "location: ".$mLinkModuleContent."&executed=edit";

     $cContenidosCampos = new Dbcontenidos_campos();
     $mId = GetData( "id", FALSE );
     
     $cContenidos = new Dbcontenidos();
     $cContenidos->setid($mId);
     $cContenidos->setactualizado(date("Y-m-d H:i:s"));
     $cContenidos->save();

     $mCampos = $cContenidosCampos->getList( array( "id_contenido" => $mId ) );

     foreach ($mCampos as $item) {
       //Llamado del campo a actualizar para validación de reglas de tipo de campo
       $cCampoDb = $cContenidosCampos->getByPk($item['id']);
       //Inicializador de objeto de base de datos
       $cCampo = new Dbcontenidos_campos();
       //Se ejecuta el setter correspondiente para indicar a DAO que se debe actualizar el registro
       $cCampo->setid($item['id']);
       
       //Si el tipo de campo que se recorre es imagen
       if ($cCampoDb['id_tipo_campo'] == 2) {
         $fileName1 = $_FILES[$item['id']]["name"];
        
         if ($fileName1 != ""){
             $destino = '../img/'.$fileName1;

             if (file_exists($destino)){
                 //unlink($destino);
             }
             
             if(move_uploaded_file($_FILES[$item['id']][ 'tmp_name' ], $destino)){
               $cCampo->settxt_valor($fileName1);
               $cCampo->save();
             }
         }
       }
       else{

         $cCampo->settxt_valor(GetData( $item['id'], FALSE ));
         $cCampo->save();
      


       }
     }

    break;
   
  }



header($location);
exit;



 ?>