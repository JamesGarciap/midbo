<?php
 include("moduleLinks.php"); 

  $mId = GetData( "id", FALSE );
  $cContenidos = new Dbcontenidos();
  $cContenidosCampos = new Dbcontenidos_campos();
  $mData = $cContenidos->getByPk($mId);
  $mCampos = $cContenidosCampos->getList( array( "id_contenido" => $mData["id"] ) );

 ?>

<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="<?php echo $mLinkDashboard ?>"><i class="icon-home"></i></a></li>
  <li><a href="<?php echo $mLinkModuleContent ?>">Content</a></li>
  <li><a href="javascript:void(0)">Edit Content</a></li>
  <li><span><?php echo utf8_encode($mData['txt_nombre']) ?></span></li>
 </ul>
</div>

 <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                   
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Edit Form</h4>
                            </div>
                            <div class="w-box-content">
                              <form id="form" name="form" action="<?php echo $mLinkModuleActions ?>&action=edit" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?php echo $mData['id'] ?>">
                                   
                                <?php if ($mCampos): ?>
                                  <?php foreach ($mCampos as $campo): ?>

                                  <?php switch ($campo['id_tipo_campo']) {
                                    case '2':
                                      $imagen = $campo['txt_valor'] != "" ? $campo['txt_valor'] : "dummy_293x80.gif";
                                      $mControl = '
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <div class="fileupload-new thumbnail" style="width: 293px; height: 86px;">
                                             <img src="../img/'.$imagen.'" alt="" >
                                            </div>
                                            <div class="fileupload-preview fileupload-exists thumbnail" style="width: 293px; height: 86px;"></div>
                                            <span class="btn btn-small btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>
                                            <input type="file" name="'.$campo['id'].'"></span>
                                            <a href="#" class="btn btn-small btn-link fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>';

                                      break;
                                    
                                    case '6':
                                      $mControl = '<textarea id="wysiwg_manager" cols="30" rows="10" name="'.$campo['id'].'">'.$campo['txt_valor'].'</textarea>';
                                      break;

                                    default:
                                      $mControl = '<input type="text" class="span8" id="s_name" name="'.$campo['id'].'" value="'.$campo['txt_valor'].'" />';
                                      break;
                                  } ?>

                                    <div class="formSep">
                                        <label><?php echo $campo['txt_titulo'] ?></label>
                                        <?php echo $mControl ?>
                                    </div>
                                   
                                    
                                  <?php endforeach ?>
                                <?php endif ?>


                                    <div class="formSep sepH_b">
                                        <input class="btn btn-beoro-3" type="submit" value="Save changes">
                                        <a href="<?php echo $mLinkModuleContent ?>" class="btn btn-danger" id="" >Cancel</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php include("moduleScripts.php") ?>
