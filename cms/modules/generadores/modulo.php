<?php

  $cActividades = new Dbtest();
  $mData = $cActividades->getByPk(2);

  $mLinkModule = Link::ToSection( array( "s" => "generadores" ) );
  $mLinksection = Link::ToSection( array( "s" => "home" ) );
  $mLinkModuleOption_generador = Link::ToSection( array( "s" => "generadores", "ss" => "generador_dao_automatico" ) );

?>
<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="javascript:void(0)"><i class="icon-home"></i></a></li>
  <li><a href="javascript:void(0)">Content</a></li>
  <li><a href="javascript:void(0)">Article: Lorem ipsum dolor...</a></li>
  <li><a href="javascript:void(0)">Comments</a></li>
  <li><span>Lorem ipsum dolor sit amet...</span></li>
 </ul>
</div>

 <div class="container">
  <div class="span4">
   <div class="w-box">
    <div class="w-box-header">
        <h4>Code</h4>
        <div class="pull-right">
            <span class="label">123</span>
        </div>
    </div>
    <div class="w-box-content">
     <pre class="prettyprint linenums inside_pre">
       <?php  var_dump($mData); ?>
     </pre>
   </div>
  </div>
 </div>

<?php echo $mData['id']; ?>

  <div class="span4">
				<a href="<?php echo $mLinkModule ?>">Generadores</a><br>
				<a href="<?php echo $mLinksection ?>">Home</a><br>
				<a href="<?php echo $mLinkModuleOption_generador ?>">dao automatico</a>

  </div>
 </div>
