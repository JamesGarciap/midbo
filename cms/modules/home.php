<?php 


    $user = GetData( "login_name", FALSE );
    $password = GetData( "login_password", FALSE );
    $logout = GetData( "logout", FALSE );
    
    if ($logout) {
     $_SESSION['user'] = NULL;
     session_destroy();
    }

    if ($user) {


        $cUsuarios = new Dbusuarios();

        $mData = $cUsuarios->getList( array( "correo" => $user, "password" => md5($password)) );

        $location = "location: ".$mLinkModuleHome."&error=noexist";
    
        if ($mData) {

          $_SESSION["user"] = $mData[0]['id'];
          $_SESSION["nombres"] = $mData[0]['nombres'];
          $_SESSION["imagen"] = $mData[0]['imagen'];
          $_SESSION["correo"] = $mData[0]['correo'];
          $location = "location: ".$mLinkModuleDashboard;

        }

        header($location);
        exit;

    }

    if (!isset($_SESSION['user'])) {
     $_SESSION['user']=NULL;
    }

    if ($_SESSION['user']!=NULL) {

       $location = "location: ".$mLinkModuleDashboard;
       header($location);
       exit;

    }

 ?>

     <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet'>


    <script type="text/javascript">
        (function(a){a.fn.vAlign=function(){return this.each(function(){var b=a(this).height(),c=a(this).outerHeight(),b=(b+(c-b))/2;a(this).css("margin-top","-"+b+"px");a(this).css("top","50%");a(this).css("position","absolute")})}})(jQuery);(function(a){a.fn.hAlign=function(){return this.each(function(){var b=a(this).width(),c=a(this).outerWidth(),b=(b+(c-b))/2;a(this).css("margin-left","-"+b+"px");a(this).css("left","50%");a(this).css("position","absolute")})}})(jQuery);
        $(document).ready(function() {
            if($('#login-wrapper').length) {
                $("#login-wrapper").vAlign().hAlign()
            };
            if($('#login-validate').length) {
                $('#login-validate').validate({
                    onkeyup: false,
                    errorClass: 'error',
                    rules: {
                        login_name: { required: true },
                        login_password: { required: true }
                    }
                })
            }
            if($('#forgot-validate').length) {
                $('#forgot-validate').validate({
                    onkeyup: false,
                    errorClass: 'error',
                    rules: {
                        forgot_email: { required: true, email: true }
                    }
                })
            }
            $('#pass_login').click(function() {
                $('.panel:visible').slideUp('200',function() {
                    $('.panel').not($(this)).slideDown('200');
                });
                $(this).children('span').toggle();
            });
        });
    </script>

      <div id="login-wrapper" class="clearfix">
        <div class="main-col">
            <img src="img/logo.png" alt="" class="logo_img" width="200" />
            <p></p>
            <div class="panel">
                <!-- <p class="heading_main">Account Login</p> -->
                <form id="login-validate" action="" method="post">
                    <label for="login_name">E-mail / Username</label>
                    <input type="text" id="login_name" name="login_name" value="" />
                    <label for="login_password">Password</label>
                    <input type="password" id="login_password" name="login_password" value="" />
<!--                     <label for="login_remember" class="checkbox"><input type="checkbox" id="login_remember" name="login_remember" /> Remember me</label>-->                    <div class="submit_sect">
                        <button type="submit" class="btn btn-beoro-3">Login</button>
                    </div>
                </form>
            </div>
            <div class="panel" style="display:none">
                <p class="heading_main">Can't sign in?</p>
                <form id="forgot-validate" method="post">
                    <label for="forgot_email">Your email adress</label>
                    <input type="text" id="forgot_email" name="forgot_email" />
                    <div class="submit_sect">
                        <button type="submit" class="btn btn-beoro-3">Request New Password</button>
                    </div>
                </form>
            </div>
        </div>
<!--         <div class="login_links">
            <a href="javascript:void(0)" id="pass_login"><span>Forgot password?</span><span style="display:none">Account login</span></a>
        </div> -->
    </div>