<?php 

    
    if ($_SESSION['user']==NULL) {

       $location = "location: ".$mLinkModuleHome;
       header($location);
       exit;

    }
 ?>

<!-- top bar -->
            <div class="navbar navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container">
                        <div class="pull-right top-search">
                            <!-- <form action="" >
                                <input type="text" name="q" id="q-main">
                                <button class="btn"><i class="icon-search"></i></button>
                            </form> -->
                        </div>
                        <div id="fade-menu" class="pull-left">
                            <ul class="clearfix" id="mobile-nav">
                                
                                <li><a href="javascript:void(0)">Components</a>
                                    <ul>
                                        <li>
                                            <!-- <a href="<?php echo $mLinkModuleCatalogo ?>">Catalog</a> -->
                                            <a href="<?php echo $mLinkModuleContent ?>">Content</a>
                                            <a href="<?php echo $mLinkModuleBanner ?>">Main Banner</a>
                                            <a href="<?php echo $mLinkModuleNoticias ?>">Noticias</a>
                                            <!-- <a href="<?php echo $mLinkModuleDao ?>">DAO</a> -->
                                        </li>
                                       
                                    </ul>
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        <!-- header -->
            <header>
                <div class="container">
                    <div class="row">
                        <div class="span3">
                            <div class="main-logo"><a href="<?php echo $mLinkDashboard ?>"><img src="img/logo.png" alt="Beoro Admin" width="70"></a></div>
                        </div>
                        <div class="span5">
                            <nav class="nav-icons">
                                <ul>
                            <!--    <li><a href="javascript:void(0)" class="ptip_s" title="Dashboard"><i class="icsw16-home"></i></a></li>
                                    <li class="dropdown">
                                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="icsw16-create-write"></i> <span class="caret"></span></a>
                                        <ul role="menu" class="dropdown-menu">
                                            <li role="presentation"><a href="#" role="menuitem">Action</a></li>
                                            <li role="presentation"><a href="#" role="menuitem">Another action</a></li>
                                            <li class="divider" role="presentation"></li>
                                            <li role="presentation"><a href="#" role="menuitem">Separated link</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="javascript:void(0)" class="ptip_s" title="Mailbox"><i class="icsw16-mail"></i><span class="badge badge-info">6</span></a></li>
                                    <li><a href="javascript:void(0)" class="ptip_s" title="Comments"><i class="icsw16-speech-bubbles"></i><span class="badge badge-important">14</span></a></li>
                                    <li class="active"><span class="ptip_s" title="Statistics (active)"><i class="icsw16-graph"></i></span></li>
                                    <li><a href="javascript:void(0)" class="ptip_s" title="Settings"><i class="icsw16-cog"></i></a></li> -->
                                </ul>
                             </nav>
                        </div>
                        <div class="span4">
                            <div class="user-box">
                                <div class="user-box-inner">
                                    <img src="img/<?php echo $_SESSION['imagen'] ?>" alt="" class="user-avatar img-avatar">
                                    <div class="user-info">
                                        Welcome, <strong><?php echo $_SESSION['nombres'] ?></strong>
                                        <ul class="unstyled">
                                            <!-- <li><a href="user_profile.html">Settings</a></li> 
                                            <li>&middot;</li>-->
                                            <li><a href="<?php echo $mLinkModuleHome ?>&logout=1">Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>