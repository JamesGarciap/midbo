
<!DOCTYPE HTML>
<html lang="en-US">
    <head>

        <meta charset="UTF-8">
        <title>CMS v1</title>
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <link rel="icon" type="image/ico" href="favicon.ico">
        
        <?php include( SITE_ROOT."modules/scripts.php");?>


    </head>
    <body class="bg_d">

    <?php
   
      //Link modules
      $mLinkDashboard = Link::ToSection( array( "s" => "dashboard" ) );
      $mLinkModuleContent = Link::ToSection( array( "s" => "content", "ss" => "overview" ) );     
      $mLinkModuleBanner = Link::ToSection( array( "s" => "banner", "ss" => "overview" ) );     
      $mLinkModuleCatalogo = Link::ToSection( array( "s" => "catalogo", "ss" => "overview" ) );     
      $mLinkModuleDao = Link::ToSection( array( "s" => "generadores", "ss" => "generador_dao_automatico" ) );     
      $mLinkModuleNoticias = Link::ToSection( array( "s" => "news", "ss" => "overview" ) );     
      
      $mConten = "home.php";

      $mSeccion = GetData( "module", FALSE );

      if( FALSE !== $mSeccion )
      {
        $mConten = Link::CleanUrlText( $mSeccion );

        //Si existe subseccion, concatenamos para generar el nombre del archivo
        $mSubseccion = GetData( "option", FALSE );

        if( FALSE !== $mSubseccion )
        {
          $mConten .= "/".Link::CleanUrlText( $mSubseccion );
        }

        // Si existe accion, concatenamos para generar el nombre del archivo
        $mAccion = GetData( "accion", FALSE );

        if( FALSE !== $mAccion )
        {
          $mConten .= "_".Link::CleanUrlText( $mAccion );
        }

        $mConten = str_replace( "-", "_", $mConten ) . ".php";
      }

      $mModal = GetData( "modal", FALSE );

      if( FALSE === $mModal )
      {
        //include( SITE_ROOT."modules/preheader.php");
    ?>

    <!-- main wrapper (without footer) -->    
        <div class="main-wrapper">
    	  <?php 
        if ($mConten != "home.php") {
        include( SITE_ROOT."modules/header.php");

        }
         ?>

          <?php
            include_once( SITE_ROOT.'modules/'.$mConten );
          ?>
        
            <div class="footer_space"></div>
        </div> 
    <?php
        include( SITE_ROOT."modules/footer.php" );
      }
      else
      {
        include_once( SITE_ROOT.'modules/'.$mConten );
      }
    ?>



    </body>
</html>