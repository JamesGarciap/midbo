<?php
 include("moduleLinks.php"); 

  $type = GetData( "type", FALSE );

 ?>

<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="<?php echo $mLinkDashboard ?>"><i class="icon-home"></i></a></li>
  <li><a href="<?php echo $mLinkModuleNoticias ?>">Noticias</a></li>
  <li><a href="javascript:void(0)">Agregar Noticias</a></li>
  <li><span>Agregar Noticias</span></li>
 </ul>
</div>

 <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                   
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Add News</h4>
                            </div>
                            <div class="w-box-content">
                              <form id="form" name="form" action="<?php echo $mLinkModuleActions ?>&action=add" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="tipo" value="<?php echo $type ?>">
                                    <div class="formSep">
                                        <label>Title</label>
                                        <input type="text" class="span8" id="txt_nombre" name="txt_nombre" value="" />
                                    </div>
                                    <div class="formSep">
                                      <label>Content</label>
                                      <textarea name="txt_descripcion" id="wysiwg_manager" cols="30" rows="10" ></textarea>   
                                    </div>

                                    <?php if ($type == "1"): ?>
                                        
                                        <div class="formSep">
                                            <label>Thumb</label>
                                            <input type="file" name="file_imagen">
                                        </div>

                                    <?php else: ?>
                                    
                                        <div class="formSep">
                                            <label>Url Link</label>
                                            <input type="text" name="url">
                                        </div>
                                    <?php endif ?>

                                    <div class="formSep">
                                         <label>State</label>
                                         <select name="estado">
                                            <option value="0">Fuera de linea</option>
                                            <option value="1">Publicado</option>
                                         </select>
                                    </div>

                                   
                                    <div class="formSep sepH_b">
                                        <input class="btn btn-beoro-3" type="submit" value="Guardar Cambios">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php include("moduleScripts.php") ?>
