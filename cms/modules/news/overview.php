<?php
 include("moduleLinks.php"); 

 $cNoticias = new Dbnoticias();
 $mData = $cNoticias->getList();
 $mAtion = GetData( "executed", FALSE );

 switch ($mAtion) {

     case 'delete':
         $mSuccess = "error";
         $mSuccessText = "Borrado ejecutado correctamente";
         break;

     case 'edit':
         $mSuccess = "success";
         $mSuccessText = "Edición ejecutada correctamente";
         break;
     
     default:
         $mSuccess = "success";
         $mSuccessText = "Contenido agregado correctamente";

         break;
 }

 ?>
<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="<?php echo $mLinkDashboard ?>"><i class="icon-home"></i></a></li>
  <li><a href="<?php echo $mLinkModuleBanner ?>">Noticias</a></li>
 </ul>
</div>

<div class="container">

<?php if ($mAtion): ?>
    
    <div class="alert alert-<?php echo $mSuccess ?>">
        <a data-dismiss="alert" class="close">×</a>
        <strong>Notificación:</strong> <?php echo $mSuccessText ?>.
    </div>

<?php endif ?>
</div>

<!-- main content -->
<div class="container">
    <div class="row-fluid">
        <div class="span12">

            <div class="w-box">
                <div class="w-box-header">
                    <div class="btn-group">
                        <a href="<?php echo $mLinkModuleAdd ?>&type=1" class="btn btn-primary btn-mini delete_rows_simple" data-tableid="smpl_tbl" title="Add Banner">Agregar Noticia</a>
                        <a href="<?php echo $mLinkModuleAdd ?>&type=0" class="btn btn-primary btn-mini delete_rows_simple" data-tableid="smpl_tbl" title="Add Banner">Agregar Video</a>
                    </div>
                </div>
                <div class="w-box-content">
                     <table id="dt_basic" class="dataTables_full table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                         <?php if ($mData): ?>
                          <?php foreach ($mData as $content): ?>
                           
                          
                            <tr>

                                <td><?php echo $content['txt_nombre'] ?></td>
                                <td>
                                    <div class="btn-group">
                                        <a href="<?php echo $mLinkModuleEdit ?>&id=<?php echo $content['id'] ?>" class="btn btn-mini" title="Edit"><i class="icon-pencil"></i></a>
                                        <a href="<?php echo $mLinkModuleActions ?>&action=delete&id=<?php echo $content['id'] ?>" class="btn btn-mini" title="Delete" onclick="return confirma(this)"><i class="icon-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                           
                          <?php endforeach ?>
                         <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- confirmation box -->

        </div>
    </div>
</div>


<?php include("moduleScripts.php") ?>