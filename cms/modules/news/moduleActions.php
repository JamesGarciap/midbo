<?php

  include("moduleLinks.php"); 

  $action = GetData( "action", FALSE );
  

  switch ($action) {

   case 'add':

     $location = "location: ".$mLinkModuleNoticias."&executed=add";
     
     $cNoticias = new Dbnoticias();

     $cNoticias->setestado(GetData( 'estado', FALSE ));
     $cNoticias->seturl(GetData( 'url', FALSE ));
     $cNoticias->settipo(GetData( 'tipo', FALSE ));
     $cNoticias->settxt_nombre(GetData( 'txt_nombre', FALSE ));
     $cNoticias->settxt_descripcion(GetData( 'txt_descripcion', FALSE ));

     $fileName1 = $_FILES['file_imagen']["name"];
    
     if ($fileName1 != ""){
         $destino = '../img/'.$fileName1;

         if (file_exists($destino)){
             //unlink($destino);
         }
         
         if(move_uploaded_file($_FILES['file_imagen'][ 'tmp_name' ], $destino)){
           $cNoticias->setfile_imagen($fileName1);
         }
     }
          
     $cNoticias->save();

    break;

   case 'delete':

     $location = "location: ".$mLinkModuleNoticias."&executed=delete";
     
     $mId = GetData( "id", FALSE );
     
     $cNoticias = new Dbnoticias();
     $cNoticias->deleteById($mId);

    break;
   
  

   case 'edit':

     $location = "location: ".$mLinkModuleNoticias."&executed=edit";
 
     $cNoticias = new Dbnoticias();
     $mId = GetData( "id", FALSE );
     $cNoticias->setid($mId);
     $cNoticias->setestado(GetData( 'estado', FALSE ));
     $cNoticias->seturl(GetData( 'url', FALSE ));
     $cNoticias->settxt_nombre(GetData( 'txt_nombre', FALSE ));
     $cNoticias->settxt_descripcion(GetData( 'txt_descripcion', FALSE ));
     $fileName1 = $_FILES['file_imagen']["name"];
    
     if ($fileName1 != ""){
         $destino = '../img/'.$fileName1;

         if (file_exists($destino)){
             //unlink($destino);
         }
         
         if(move_uploaded_file($_FILES['file_imagen'][ 'tmp_name' ], $destino)){
           $cNoticias->setfile_imagen($fileName1);
         }
     }
          
     $cNoticias->save();
    break;
   
  }


header($location);
exit;



 ?>