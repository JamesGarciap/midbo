<?php
 include("moduleLinks.php"); 

  $mId = GetData( "id", FALSE );
  $cNoticias = new Dbnoticias();
  $mData = $cNoticias->getByPk($mId);

 ?>

<!-- breadcrumbs -->
<div class="container">
 <ul id="breadcrumbs">
  <li><a href="<?php echo $mLinkDashboard ?>"><i class="icon-home"></i></a></li>
  <li><a href="<?php echo $mLinkModuleNoticias ?>">Noticias</a></li>
  <li><a href="javascript:void(0)">Editar Noticias</a></li>
  <li><span><?php echo utf8_encode($mData['txt_nombre']) ?></span></li>
 </ul>
</div>

 <!-- main content -->
            <div class="container">
                <div class="row-fluid">
                   
                    <div class="span12">
                        <div class="w-box">
                            <div class="w-box-header">
                                <h4>Edit News</h4>
                            </div>
                            <div class="w-box-content">
                              <form id="form" name="form" action="<?php echo $mLinkModuleActions ?>&action=edit" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?php echo $mData['id'] ?>">


                                    <div class="formSep">
                                        <label>Title</label>
                                        <input type="text" class="span8" id="s_name" name="txt_nombre" value="<?php echo $mData['txt_nombre'] ?>" />
                                    </div>
                                    
                                    <div class="formSep">
                                      <label>Content</label>
                                      <textarea name="txt_descripcion" id="wysiwg_manager" cols="30" rows="10" ><?php echo $mData['txt_descripcion'] ?></textarea>   
                                    </div>
                                    
                                    <?php if ($mData['tipo']==1): ?>
                                        
                                      <div class="formSep">
                                          <label>Thumb</label>
                                          <input type="file" name="file_imagen">
                                      </div>

                                    <?php else: ?>

                                      <div class="formSep">
                                            <label>Url Link</label>
                                            <input type="text" name="url" value="<?php echo $mData['url'] ?>">
                                      </div>
                                    <?php endif ?>

                                    <div class="formSep">
                                         <label>State</label>
                                         <select name="estado">
                                            <option value="0" <?php echo $mData['estado']==0 ? "selected" : "" ; ?> >Fuera de linea</option>
                                            <option value="1" <?php echo $mData['estado']==1 ? "selected" : "" ; ?> >Publicado</option>
                                         </select>
                                    </div>


                                    <div class="formSep sepH_b">
                                        <input class="btn btn-beoro-3" type="submit" value="Guardar Cambios">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php include("moduleScripts.php") ?>
