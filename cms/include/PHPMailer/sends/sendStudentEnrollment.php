<?php

//POST Data


$par1 = $_POST['par1'];
$par2 = $_POST['par2'];
$par3 = $_POST['par3'];
$par4 = $_POST['par4'];
$par5 = $_POST['par5'];
$fname = $_POST['fname'];
$mname = $_POST['mname'];
$lname = $_POST['lname'];
$scode = $_POST['scode'];
$grade = $_POST['grade'];
$mm = $_POST['mm'];
$dd = $_POST['dd'];
$yyyy = $_POST['yyyy'];
$fcode = $_POST['fcode'];
$email = $_POST['email'];

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require '../PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer();
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "james.garcia@imaginamos.com.co";
//Password to use for SMTP authentication
$mail->Password = "hsxb7twg";
//Set who the message is to be sent from
$mail->setFrom('james.garcia@imaginamos.com.co', 'James Garcia');
//Set who the message is to be sent to
$mail->addAddress('james.jgarciap@gmail.com', 'James Garcia');
//Set the subject line
$mail->Subject = 'Fodd Services - Student Enrollment: '.$scode;

//Cuerpo del mensaje basado en ./mailGeneralTemplate.php
$message = '<html><body>';
$message .= '<div style="width: 640px; font-family: Arial, Helvetica, sans-serif; ">';
$message .= '<img src="http://www.cng.edu/mailer/mail_cabezote.jpg" alt="Website Email" />';
//Titulo Principal
$message .= '<h3 style="margin-left:20px;color:#0076A5;">Fodd Services - Student Enrollment</h3>';

//Inicio Tabla de información familiar
$message .= '<h4 style="margin-left:20px;color:#0076A5;">Please take info account the following Information:</h4>';
//HTML para parrafos
$message .= '<div style="width: 550px; margin-left:25px;text-aling:justify; ">';
$message .= "<p style='font-size: 11px;'>Any change in the Cafeteria Service must be notifield at least 5 days before the billing of the next cycle. The dates estabilished for invoice issuance for the 2014 - 2015 school year are: ".$par1.", ".$par2.", ".$par3.", ".$par4." and ".$par5." .</p>";
$message .= "</div>";
//Fin Tabla
$message .= '<table rules="all" style="border-color: #666;margin-left:20px;width:550px;font-size: 11px;" cellpadding="10">';
//Celdas por defecto
$message .= "<tr><td><strong>Full Name:</strong> </td><td>".$fname." ".$mname." ".$lname."</td></tr>";
$message .= "<tr><td><strong>Student Code:</strong> </td><td>".$scode."</td></tr>";
$message .= "<tr><td><strong>Grade:</strong> </td><td>".$grade."</td></tr>";
$message .= "<tr><td><strong>Date of enrollment:</strong> </td><td>".$mm." ".$dd." of ".$yyyy."</td></tr>";
$message .= "<tr><td><strong>Family Code</strong> </td><td>".$fcode."</td></tr>";
$message .= "<tr><td><strong>E-mail:</strong> </td><td>".$email."</td></tr>";
$message .= "</table>";
//Fin Tabla



$message .= "</div>";
$message .= "</body></html>";


//Read an HTML message body from an external file, convert referenced images to embedded,
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
$mail->msgHTML($message);
//Replace the plain text body with one created manually
//$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.gif');

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {

    $location = "location: ./../../../index.php?seccion=student_enrollment_form&add=1";
	header($location);
	exit;
}



?>