<?php

//POST Data

$inquieryis = $_POST['inquieryis'];
$phone_number = $_POST['phone_number'];
$email1 = $_POST['email1'];
$email2 = $_POST['email2'];
$question = $_POST['question'];
$contact = $_POST['contact'];


//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require '../PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer();
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "james.garcia@imaginamos.com.co";
//Password to use for SMTP authentication
$mail->Password = "hsxb7twg";
//Set who the message is to be sent from
$mail->setFrom('james.garcia@imaginamos.com.co', 'James Garcia');
//Set who the message is to be sent to
$mail->addAddress('james.jgarciap@gmail.com', 'James Garcia');
//Set the subject line
$mail->Subject = 'New Families - Help Desk: '.$email1;

//Cuerpo del mensaje basado en ./mailGeneralTemplate.php
$message = '<html><body>';
$message .= '<div style="width: 640px; font-family: Arial, Helvetica, sans-serif; ">';
$message .= '<img src="http://www.cng.edu/mailer/mail_cabezote.jpg" alt="Website Email" />';
//Titulo Principal
$message .= '<h3 style="margin-left:20px;color:#0076A5;">New Families - Help Desk</h3>';

//Inicio Tabla de información familiar
$message .= '<table rules="all" style="border-color: #666;margin-left:20px;width:550px;font-size: 11px;" cellpadding="10">';
//Celdas por defecto
$message .= "<tr><td><strong>My inquiry is regarding:</strong> </td><td>".$inquieryis."</td></tr>";
$message .= "<tr><td><strong>Phone number:</strong> </td><td>".$phone_number."</td></tr>";
$message .= "<tr><td><strong>Primary e-mail:</strong> </td><td>".$email1."</td></tr>";
$message .= "<tr><td><strong>Secondary e-mail:</strong> </td><td>".$email2."</td></tr>";
$message .= "<tr><td><strong>E-mail:</strong> </td><td>".$contact."</td></tr>";
$message .= "</table>";
//Fin Tabla

$message .= '<h4 style="margin-left:20px;color:#0076A5;">Question:</h4>';
//HTML para parrafos
$message .= '<div style="width: 550px; margin-left:25px;text-aling:justify; ">';
$message .= "<p style='font-size: 11px;'>".$question."</p>";
$message .= "</div>";
//Fin Tabla


$message .= "</div>";
$message .= "</body></html>";


//Read an HTML message body from an external file, convert referenced images to embedded,
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
$mail->msgHTML($message);
//Replace the plain text body with one created manually
//$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.gif');

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {

    $location = "location: ./../../../index.php?seccion=new_families_faq&add=1";
	header($location);
	exit;
}



?>