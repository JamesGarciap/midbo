<?php

//POST Data

//Family information
$codeFamily = $_POST['codeFamily'];
$nameFamily = $_POST['nameFamily'];
$emailFamily = $_POST['emailFamily'];
//Car one information
$carOne = $_POST['carOne'];
$carOneModel = $_POST['carOneModel'];
$carOneColor = $_POST['carOneColor'];
$carOnePlate = $_POST['carOnePlate'];
//Car two information
$carTwo = $_POST['carTwo'];
$carTwoModel = $_POST['carTwoModel'];
$carTwoColor = $_POST['carTwoColor'];
$carTwoPlate = $_POST['carTwoPlate'];
//Car three information
$carThree = $_POST['carThree'];
$carThreeModel = $_POST['carThreeModel'];
$carThreeColor = $_POST['carThreeColor'];
$carThreePlate = $_POST['carThreePlate'];

//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

require '../PHPMailerAutoload.php';

//Create a new PHPMailer instance
$mail = new PHPMailer();
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "james.garcia@imaginamos.com.co";
//Password to use for SMTP authentication
$mail->Password = "hsxb7twg";
//Set who the message is to be sent from
$mail->setFrom('james.garcia@imaginamos.com.co', 'James Garcia');
//Set who the message is to be sent to
$mail->addAddress('james.jgarciap@gmail.com', 'James Garcia');
//Set the subject line
$mail->Subject = 'Car Registration Parents - Code: '.$codeFamily;

//Cuerpo del mensaje basado en ./mailGeneralTemplate.php
$message = '<html><body>';
$message .= '<div style="width: 640px; font-family: Arial, Helvetica, sans-serif; ">';
$message .= '<img src="http://www.cng.edu/mailer/mail_cabezote.jpg" alt="Website Email" />';
//Titulo Principal
$message .= '<h3 style="margin-left:20px;color:#0076A5;">Transportation::Car Registration</h3>';

//Inicio Tabla de información familiar
$message .= '<h4 style="margin-left:20px;color:#0076A5;">Family information</h4>';
$message .= '<table rules="all" style="border-color: #666;margin-left:20px;width:550px;font-size: 11px;" cellpadding="10">';
//Celdas por defecto
$message .= "<tr><td><strong>Family Code:</strong> </td><td>".$codeFamily."</td></tr>";
$message .= "<tr><td><strong>Family Name(s):</strong> </td><td>".$nameFamily."</td></tr>";
$message .= "<tr><td><strong>E-mail:</strong> </td><td>".$emailFamily."</td></tr>";
$message .= "</table>";
//Fin Tabla


//Inicio Tabla de información familiar
$message .= '<h4 style="margin-left:20px;color:#0076A5;">Car One information</h4>';
$message .= '<table rules="all" style="border-color: #666;margin-left:20px;width:550px;font-size: 11px;" cellpadding="10">';
//Celdas por defecto
$message .= "<tr><td><strong>Reference:</strong> </td><td>".$carOne."</td></tr>";
$message .= "<tr><td><strong>Model:</strong> </td><td>".$carOneModel."</td></tr>";
$message .= "<tr><td><strong>Color:</strong> </td><td>".$carOneColor."</td></tr>";
$message .= "<tr><td><strong>License Plate:</strong> </td><td>".$carOnePlate."</td></tr>";
$message .= "</table>";
//Fin Tabla
//Inicio Tabla de información familiar
$message .= '<h4 style="margin-left:20px;color:#0076A5;">Car Two information</h4>';
$message .= '<table rules="all" style="border-color: #666;margin-left:20px;width:550px;font-size: 11px;" cellpadding="10">';
//Celdas por defecto
$message .= "<tr><td><strong>Reference:</strong> </td><td>".$carTwo."</td></tr>";
$message .= "<tr><td><strong>Model:</strong> </td><td>".$carTwoModel."</td></tr>";
$message .= "<tr><td><strong>Color:</strong> </td><td>".$carTwoColor."</td></tr>";
$message .= "<tr><td><strong>License Plate:</strong> </td><td>".$carTwoPlate."</td></tr>";
$message .= "</table>";
//Fin Tabla
//Inicio Tabla de información familiar
$message .= '<h4 style="margin-left:20px;color:#0076A5;">Car Three information</h4>';
$message .= '<table rules="all" style="border-color: #666;margin-left:20px;width:550px;font-size: 11px;" cellpadding="10">';
//Celdas por defecto
$message .= "<tr><td><strong>Reference:</strong> </td><td>".$carThree."</td></tr>";
$message .= "<tr><td><strong>Model:</strong> </td><td>".$carThreeModel."</td></tr>";
$message .= "<tr><td><strong>Color:</strong> </td><td>".$carThreeColor."</td></tr>";
$message .= "<tr><td><strong>License Plate:</strong> </td><td>".$carThreePlate."</td></tr>";
$message .= "</table>";
//Fin Tabla

$message .= "</div>";
$message .= "</body></html>";


//Read an HTML message body from an external file, convert referenced images to embedded,
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
$mail->msgHTML($message);
//Replace the plain text body with one created manually
//$mail->AltBody = 'This is a plain-text message body';
//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.gif');

//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {

    $location = "location: ./../../../index.php?seccion=car_registration_parents&add=1";
	header($location);
	exit;
}



?>