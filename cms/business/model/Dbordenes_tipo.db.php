<?php
/*
 * @file               : Dbordenes_tipo.db.php
 * @brief              : Clase para la interaccion con la tabla ordenes_tipo
 * @version            : 3.3
 * @ultima_modificacion: 2014-03-29
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbordenes_tipo
 * @brief: Clase para la interaccion con la tabla ordenes_tipo
 */
 
class Dbordenes_tipo extends DbDAO {

  public $id = NULL;
  protected $tipo = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function settipo($mData = NULL) {
    if ($mData === NULL) { $this->tipo = NULL; }
    $this->tipo = StripHtml($mData);
  }

}
?>