<?php
/*
 * @file               : Dbpayu_configuration.db.php
 * @brief              : Clase para la interaccion con la tabla payu_configuration
 * @version            : 3.3
 * @ultima_modificacion: 2014-03-30
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbpayu_configuration
 * @brief: Clase para la interaccion con la tabla payu_configuration
 */
 
class Dbpayu_configuration extends DbDAO {

  public $id = NULL;
  protected $accountid = NULL;
  protected $api_key = NULL;
  protected $api_login = NULL;
  protected $public_key = NULL;
  protected $commerce_id = NULL;
  protected $gateway = NULL;
  protected $test = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function setaccountid($mData = NULL) {
    if ($mData === NULL) { $this->accountid = NULL; }
    $this->accountid = StripHtml($mData);
  }

  public function setapi_key($mData = NULL) {
    if ($mData === NULL) { $this->api_key = NULL; }
    $this->api_key = StripHtml($mData);
  }

  public function setapi_login($mData = NULL) {
    if ($mData === NULL) { $this->api_login = NULL; }
    $this->api_login = StripHtml($mData);
  }

  public function setpublic_key($mData = NULL) {
    if ($mData === NULL) { $this->public_key = NULL; }
    $this->public_key = StripHtml($mData);
  }

  public function setcommerce_id($mData = NULL) {
    if ($mData === NULL) { $this->commerce_id = NULL; }
    $this->commerce_id = StripHtml($mData);
  }

  public function setgateway($mData = NULL) {
    if ($mData === NULL) { $this->gateway = NULL; }
    $this->gateway = StripHtml($mData);
  }

  public function settest($mData = NULL) {
    if ($mData === NULL) { $this->test = NULL; }
    $this->test = StripHtml($mData);
  }

}
?>