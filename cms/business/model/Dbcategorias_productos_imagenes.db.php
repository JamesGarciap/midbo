<?php
/*
 * @file               : Dbcategorias_productos_imagenes.db.php
 * @brief              : Clase para la interaccion con la tabla categorias_productos_imagenes
 * @version            : 3.3
 * @ultima_modificacion: 2014-03-09
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbcategorias_productos_imagenes
 * @brief: Clase para la interaccion con la tabla categorias_productos_imagenes
 */
 
class Dbcategorias_productos_imagenes extends DbDAO {

  public $id = NULL;
  protected $imagen = NULL;
  protected $id_producto = NULL;
  protected $tipo = NULL;
  protected $txt_nombre = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function setimagen($mData = NULL) {
    if ($mData === NULL) { $this->imagen = NULL; }
    $this->imagen = StripHtml($mData);
  }

  public function setid_producto($mData = NULL) {
    if ($mData === NULL) { $this->id_producto = NULL; }
    $this->id_producto = StripHtml($mData);
  }

  public function settipo($mData = NULL) {
    if ($mData === NULL) { $this->tipo = NULL; }
    $this->tipo = StripHtml($mData);
  }

  public function settxt_nombre($mData = NULL) {
    if ($mData === NULL) { $this->txt_nombre = NULL; }
    $this->txt_nombre = StripHtml($mData);
  }

}
?>