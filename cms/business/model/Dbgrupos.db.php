<?php
/*
 * @file               : Dbgrupos.db.php
 * @brief              : Clase para la interaccion con la tabla grupos
 * @version            : 3.3
 * @ultima_modificacion: 2014-04-15
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbgrupos
 * @brief: Clase para la interaccion con la tabla grupos
 */
 
class Dbgrupos extends DbDAO {

  public $id = NULL;
  protected $grupo = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function setgrupo($mData = NULL) {
    if ($mData === NULL) { $this->grupo = NULL; }
    $this->grupo = StripHtml($mData);
  }

}
?>