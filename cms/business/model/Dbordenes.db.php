<?php
/*
 * @file               : Dbordenes.db.php
 * @brief              : Clase para la interaccion con la tabla ordenes
 * @version            : 3.3
 * @ultima_modificacion: 2014-03-29
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbordenes
 * @brief: Clase para la interaccion con la tabla ordenes
 */
 
class Dbordenes extends DbDAO {

  public $id = NULL;
  protected $ordenes_tipo_id = NULL;
  protected $ordenes_estado_id = NULL;
  protected $clientes_id = NULL;
  protected $codigo = NULL;
  protected $fecha = NULL;
  protected $fecha_envio = NULL;
  protected $fecha_entrega = NULL;
  protected $fecha_actualizado = NULL;
  protected $observacion_envio = NULL;
  protected $observacion_entrega = NULL;
  protected $amount = NULL;
  protected $tax = NULL;
  protected $tax_return_base = NULL;
  protected $signature = NULL;
  protected $currency = NULL;
  protected $state_pol = NULL;
  protected $risk = NULL;
  protected $response_code_pol = NULL;
  protected $reference_sale = NULL;
  protected $reference_pol = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function setordenes_tipo_id($mData = NULL) {
    if ($mData === NULL) { $this->ordenes_tipo_id = NULL; }
    $this->ordenes_tipo_id = StripHtml($mData);
  }

  public function setordenes_estado_id($mData = NULL) {
    if ($mData === NULL) { $this->ordenes_estado_id = NULL; }
    $this->ordenes_estado_id = StripHtml($mData);
  }

  public function setclientes_id($mData = NULL) {
    if ($mData === NULL) { $this->clientes_id = NULL; }
    $this->clientes_id = StripHtml($mData);
  }

  public function setcodigo($mData = NULL) {
    if ($mData === NULL) { $this->codigo = NULL; }
    $this->codigo = StripHtml($mData);
  }

  public function setfecha($mData = NULL) {
    if ($mData === NULL) { $this->fecha = NULL; }
    $this->fecha = StripHtml($mData);
  }

  public function setfecha_envio($mData = NULL) {
    if ($mData === NULL) { $this->fecha_envio = NULL; }
    $this->fecha_envio = StripHtml($mData);
  }

  public function setfecha_entrega($mData = NULL) {
    if ($mData === NULL) { $this->fecha_entrega = NULL; }
    $this->fecha_entrega = StripHtml($mData);
  }

  public function setfecha_actualizado($mData = NULL) {
    if ($mData === NULL) { $this->fecha_actualizado = NULL; }
    $this->fecha_actualizado = StripHtml($mData);
  }

  public function setobservacion_envio($mData = NULL) {
    if ($mData === NULL) { $this->observacion_envio = NULL; }
    $this->observacion_envio = StripHtml($mData);
  }

  public function setobservacion_entrega($mData = NULL) {
    if ($mData === NULL) { $this->observacion_entrega = NULL; }
    $this->observacion_entrega = StripHtml($mData);
  }

  public function setamount($mData = NULL) {
    if ($mData === NULL) { $this->amount = NULL; }
    $this->amount = StripHtml($mData);
  }

  public function settax($mData = NULL) {
    if ($mData === NULL) { $this->tax = NULL; }
    $this->tax = StripHtml($mData);
  }

  public function settax_return_base($mData = NULL) {
    if ($mData === NULL) { $this->tax_return_base = NULL; }
    $this->tax_return_base = StripHtml($mData);
  }

  public function setsignature($mData = NULL) {
    if ($mData === NULL) { $this->signature = NULL; }
    $this->signature = StripHtml($mData);
  }

  public function setcurrency($mData = NULL) {
    if ($mData === NULL) { $this->currency = NULL; }
    $this->currency = StripHtml($mData);
  }

  public function setstate_pol($mData = NULL) {
    if ($mData === NULL) { $this->state_pol = NULL; }
    $this->state_pol = StripHtml($mData);
  }

  public function setrisk($mData = NULL) {
    if ($mData === NULL) { $this->risk = NULL; }
    $this->risk = StripHtml($mData);
  }

  public function setresponse_code_pol($mData = NULL) {
    if ($mData === NULL) { $this->response_code_pol = NULL; }
    $this->response_code_pol = StripHtml($mData);
  }

  public function setreference_sale($mData = NULL) {
    if ($mData === NULL) { $this->reference_sale = NULL; }
    $this->reference_sale = StripHtml($mData);
  }

  public function setreference_pol($mData = NULL) {
    if ($mData === NULL) { $this->reference_pol = NULL; }
    $this->reference_pol = StripHtml($mData);
  }

}
?>