<?php
/*
 * @file               : Dbusuarios.db.php
 * @brief              : Clase para la interaccion con la tabla usuarios
 * @version            : 3.3
 * @ultima_modificacion: 2014-04-10
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbusuarios
 * @brief: Clase para la interaccion con la tabla usuarios
 */
 
class Dbusuarios extends DbDAO {

  public $id = NULL;
  protected $correo = NULL;
  protected $nombres = NULL;
  protected $imagen = NULL;
  protected $password = NULL;
  protected $tipo = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function setcorreo($mData = NULL) {
    if ($mData === NULL) { $this->correo = NULL; }
    $this->correo = StripHtml($mData);
  }

  public function setnombres($mData = NULL) {
    if ($mData === NULL) { $this->nombres = NULL; }
    $this->nombres = StripHtml($mData);
  }

  public function setimagen($mData = NULL) {
    if ($mData === NULL) { $this->imagen = NULL; }
    $this->imagen = StripHtml($mData);
  }

  public function setpassword($mData = NULL) {
    if ($mData === NULL) { $this->password = NULL; }
    $this->password = StripHtml($mData);
  }

  public function settipo($mData = NULL) {
    if ($mData === NULL) { $this->tipo = NULL; }
    $this->tipo = StripHtml($mData);
  }

}
?>