<?php
/*
 * @file               : Dbtipo_campo.db.php
 * @brief              : Clase para la interaccion con la tabla tipo_campo
 * @version            : 3.3
 * @ultima_modificacion: 2014-03-01
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbtipo_campo
 * @brief: Clase para la interaccion con la tabla tipo_campo
 */
 
class Dbtipo_campo extends DbDAO {

  public $id = NULL;
  protected $txt_nombre = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function settxt_nombre($mData = NULL) {
    if ($mData === NULL) { $this->txt_nombre = NULL; }
    $this->txt_nombre = StripHtml($mData);
  }

}
?>