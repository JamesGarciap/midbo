<?php
/*
 * @file               : Dbordenes_productos.db.php
 * @brief              : Clase para la interaccion con la tabla ordenes_productos
 * @version            : 3.3
 * @ultima_modificacion: 2014-03-29
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbordenes_productos
 * @brief: Clase para la interaccion con la tabla ordenes_productos
 */
 
class Dbordenes_productos extends DbDAO {

  public $id = NULL;
  protected $ordenes_id = NULL;
  protected $categorias_productos_id = NULL;
  protected $cantidad = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function setordenes_id($mData = NULL) {
    if ($mData === NULL) { $this->ordenes_id = NULL; }
    $this->ordenes_id = StripHtml($mData);
  }

  public function setcategorias_productos_id($mData = NULL) {
    if ($mData === NULL) { $this->categorias_productos_id = NULL; }
    $this->categorias_productos_id = StripHtml($mData);
  }

  public function setcantidad($mData = NULL) {
    if ($mData === NULL) { $this->cantidad = NULL; }
    $this->cantidad = StripHtml($mData);
  }

}
?>