<?php
/*
 * @file               : Dbcategorias_productos.db.php
 * @brief              : Clase para la interaccion con la tabla categorias_productos
 * @version            : 3.3
 * @ultima_modificacion: 2014-05-02
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbcategorias_productos
 * @brief: Clase para la interaccion con la tabla categorias_productos
 */
 
class Dbcategorias_productos extends DbDAO {

  public $id = NULL;
  protected $txt_nombre = NULL;
  protected $txt_texto = NULL;
  protected $txt_caracteristicas = NULL;
  protected $imagen = NULL;
  protected $render = NULL;
  protected $id_categoria = NULL;
  protected $actualizado = NULL;
  protected $archivo = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function settxt_nombre($mData = NULL) {
    if ($mData === NULL) { $this->txt_nombre = NULL; }
    $this->txt_nombre = StripHtml($mData);
  }

  public function settxt_texto($mData = NULL) {
    if ($mData === NULL) { $this->txt_texto = NULL; }
    $this->txt_texto = StripHtml($mData);
  }

  public function settxt_caracteristicas($mData = NULL) {
    if ($mData === NULL) { $this->txt_caracteristicas = NULL; }
    $this->txt_caracteristicas = StripHtml($mData);
  }

  public function setimagen($mData = NULL) {
    if ($mData === NULL) { $this->imagen = NULL; }
    $this->imagen = StripHtml($mData);
  }

  public function setrender($mData = NULL) {
    if ($mData === NULL) { $this->render = NULL; }
    $this->render = StripHtml($mData);
  }

  public function setid_categoria($mData = NULL) {
    if ($mData === NULL) { $this->id_categoria = NULL; }
    $this->id_categoria = StripHtml($mData);
  }

  public function setactualizado($mData = NULL) {
    if ($mData === NULL) { $this->actualizado = NULL; }
    $this->actualizado = StripHtml($mData);
  }

  public function setarchivo($mData = NULL) {
    if ($mData === NULL) { $this->archivo = NULL; }
    $this->archivo = StripHtml($mData);
  }

}
?>