<?php
/*
 * @file               : Dbnoticias.db.php
 * @brief              : Clase para la interaccion con la tabla noticias
 * @version            : 3.3
 * @ultima_modificacion: 2014-06-15
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbnoticias
 * @brief: Clase para la interaccion con la tabla noticias
 */
 
class Dbnoticias extends DbDAO {

  public $id = NULL;
  protected $txt_nombre = NULL;
  protected $txt_descripcion = NULL;
  protected $estado = NULL;
  protected $file_imagen = NULL;
  protected $url = NULL;
  protected $tipo = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function settxt_nombre($mData = NULL) {
    if ($mData === NULL) { $this->txt_nombre = NULL; }
    $this->txt_nombre = StripHtml($mData);
  }

  public function settxt_descripcion($mData = NULL) {
    if ($mData === NULL) { $this->txt_descripcion = NULL; }
    $this->txt_descripcion = StripHtml($mData);
  }

  public function setestado($mData = NULL) {
    if ($mData === NULL) { $this->estado = NULL; }
    $this->estado = StripHtml($mData);
  }

  public function setfile_imagen($mData = NULL) {
    if ($mData === NULL) { $this->file_imagen = NULL; }
    $this->file_imagen = StripHtml($mData);
  }

  public function seturl($mData = NULL) {
    if ($mData === NULL) { $this->url = NULL; }
    $this->url = StripHtml($mData);
  }

  public function settipo($mData = NULL) {
    if ($mData === NULL) { $this->tipo = NULL; }
    $this->tipo = StripHtml($mData);
  }

}
?>