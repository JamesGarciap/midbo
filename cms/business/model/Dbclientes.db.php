<?php
/*
 * @file               : Dbclientes.db.php
 * @brief              : Clase para la interaccion con la tabla clientes
 * @version            : 3.3
 * @ultima_modificacion: 2014-03-29
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbclientes
 * @brief: Clase para la interaccion con la tabla clientes
 */
 
class Dbclientes extends DbDAO {

  public $id = NULL;
  protected $nombres = NULL;
  protected $email = NULL;
  protected $ciudad = NULL;
  protected $direccion = NULL;
  protected $telefono = NULL;
  protected $celular = NULL;
  protected $observaciones = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function setnombres($mData = NULL) {
    if ($mData === NULL) { $this->nombres = NULL; }
    $this->nombres = StripHtml($mData);
  }

  public function setemail($mData = NULL) {
    if ($mData === NULL) { $this->email = NULL; }
    $this->email = StripHtml($mData);
  }

  public function setciudad($mData = NULL) {
    if ($mData === NULL) { $this->ciudad = NULL; }
    $this->ciudad = StripHtml($mData);
  }

  public function setdireccion($mData = NULL) {
    if ($mData === NULL) { $this->direccion = NULL; }
    $this->direccion = StripHtml($mData);
  }

  public function settelefono($mData = NULL) {
    if ($mData === NULL) { $this->telefono = NULL; }
    $this->telefono = StripHtml($mData);
  }

  public function setcelular($mData = NULL) {
    if ($mData === NULL) { $this->celular = NULL; }
    $this->celular = StripHtml($mData);
  }

  public function setobservaciones($mData = NULL) {
    if ($mData === NULL) { $this->observaciones = NULL; }
    $this->observaciones = StripHtml($mData);
  }

}
?>