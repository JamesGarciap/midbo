<?php
/*
 * @file               : Dbordenes_estado.db.php
 * @brief              : Clase para la interaccion con la tabla ordenes_estado
 * @version            : 3.3
 * @ultima_modificacion: 2014-03-29
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbordenes_estado
 * @brief: Clase para la interaccion con la tabla ordenes_estado
 */
 
class Dbordenes_estado extends DbDAO {

  public $id = NULL;
  protected $estado = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function setestado($mData = NULL) {
    if ($mData === NULL) { $this->estado = NULL; }
    $this->estado = StripHtml($mData);
  }

}
?>