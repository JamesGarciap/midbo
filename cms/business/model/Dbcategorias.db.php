<?php
/*
 * @file               : Dbcategorias.db.php
 * @brief              : Clase para la interaccion con la tabla categorias
 * @version            : 3.3
 * @ultima_modificacion: 2014-05-02
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbcategorias
 * @brief: Clase para la interaccion con la tabla categorias
 */
 
class Dbcategorias extends DbDAO {

  public $id = NULL;
  protected $txt_nombre = NULL;
  protected $actualizado = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function settxt_nombre($mData = NULL) {
    if ($mData === NULL) { $this->txt_nombre = NULL; }
    $this->txt_nombre = StripHtml($mData);
  }

  public function setactualizado($mData = NULL) {
    if ($mData === NULL) { $this->actualizado = NULL; }
    $this->actualizado = StripHtml($mData);
  }

}
?>