<?php
/*
 * @file               : Dbcontenidos_campos.db.php
 * @brief              : Clase para la interaccion con la tabla contenidos_campos
 * @version            : 3.3
 * @ultima_modificacion: 2014-03-01
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbcontenidos_campos
 * @brief: Clase para la interaccion con la tabla contenidos_campos
 */
 
class Dbcontenidos_campos extends DbDAO {

  public $id = NULL;
  protected $id_contenido = NULL;
  protected $id_tipo_campo = NULL;
  protected $txt_titulo = NULL;
  protected $txt_valor = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function setid_contenido($mData = NULL) {
    if ($mData === NULL) { $this->id_contenido = NULL; }
    $this->id_contenido = StripHtml($mData);
  }

  public function setid_tipo_campo($mData = NULL) {
    if ($mData === NULL) { $this->id_tipo_campo = NULL; }
    $this->id_tipo_campo = StripHtml($mData);
  }

  public function settxt_titulo($mData = NULL) {
    if ($mData === NULL) { $this->txt_titulo = NULL; }
    $this->txt_titulo = StripHtml($mData);
  }

  public function settxt_valor($mData = NULL) {
    if ($mData === NULL) { $this->txt_valor = NULL; }
    $this->txt_valor = StripHtml($mData);
  }

}
?>