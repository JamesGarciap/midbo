<?php
/*
 * @file               : Dbbanner.db.php
 * @brief              : Clase para la interaccion con la tabla banner
 * @version            : 3.3
 * @ultima_modificacion: 2014-03-02
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbbanner
 * @brief: Clase para la interaccion con la tabla banner
 */
 
class Dbbanner extends DbDAO {

  public $id = NULL;
  protected $imagen = NULL;
  protected $titulo = NULL;
  protected $texto = NULL;
  protected $enlace = NULL;
  protected $actualizado = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function setimagen($mData = NULL) {
    if ($mData === NULL) { $this->imagen = NULL; }
    $this->imagen = StripHtml($mData);
  }

  public function settitulo($mData = NULL) {
    if ($mData === NULL) { $this->titulo = NULL; }
    $this->titulo = StripHtml($mData);
  }

  public function settexto($mData = NULL) {
    if ($mData === NULL) { $this->texto = NULL; }
    $this->texto = StripHtml($mData);
  }

  public function setenlace($mData = NULL) {
    if ($mData === NULL) { $this->enlace = NULL; }
    $this->enlace = StripHtml($mData);
  }

  public function setactualizado($mData = NULL) {
    if ($mData === NULL) { $this->actualizado = NULL; }
    $this->actualizado = StripHtml($mData);
  }

}
?>