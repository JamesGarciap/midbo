<?php
/*
 * @file               : Dbcontenidos.db.php
 * @brief              : Clase para la interaccion con la tabla contenidos
 * @version            : 3.3
 * @ultima_modificacion: 2014-03-17
 * @author             : Ruben Dario Cifuentes Torres
 * @generated          : Generador DAO version 1.1 
 *
 * @class: Dbcontenidos
 * @brief: Clase para la interaccion con la tabla contenidos
 */
 
class Dbcontenidos extends DbDAO {

  public $id = NULL;
  protected $txt_nombre = NULL;
  protected $actualizado = NULL;

  public function setid($mData = NULL) {
    if ($mData === NULL) { $this->id = NULL; }
    $this->id = StripHtml($mData);
  }

  public function settxt_nombre($mData = NULL) {
    if ($mData === NULL) { $this->txt_nombre = NULL; }
    $this->txt_nombre = StripHtml($mData);
  }

  public function setactualizado($mData = NULL) {
    if ($mData === NULL) { $this->actualizado = NULL; }
    $this->actualizado = StripHtml($mData);
  }

}
?>