<?php


class Carrito{

  public $num_productos = 0; 
  public $array_id_prod = NULL;
  public $array_muestra_prod = NULL; 
  public $array_cantidad_prod = NULL;
  public $array_tipo = NULL;
  public $array_class = NULL;
  public $array_guest = NULL;

  function introduce_producto($id_prod,$muestra_prod,$cantidad_prod,$tipo,$clase,$guest){
    $this->array_id_prod[$this->num_productos]=$id_prod;
    $this->array_muestra_prod[$this->num_productos]=$muestra_prod;
    $this->array_cantidad_prod[$this->num_productos]=$cantidad_prod;
    $this->array_tipo[$this->num_productos]=$tipo;
    $this->array_class[$this->num_productos]=$clase;
    $this->array_guest[$this->num_productos]=$guest;
    $this->num_productos++;
  } 


  function imprimeTotalProductos(){ 
    $total =0;
    $cantidad =0;
    $id =0;
    for ($i=0;$i<$this->num_productos;$i++){ 
      if($this->array_id_prod[$i]!=0){ 
        
        $id= $this->array_id_prod[$i];
        $cantidad = $this->array_cantidad_prod[$i];

        $total = (int)$total + (int)$cantidad;
     }
    }
    echo number_format($total);
  }



  public function imprime_carritoGrande(){ 
    $subtotal = 0;
    $iva = 0;
    $totalFinal = 0;
    $totalProductos = 0;
    $printCarrito = '<div class="cont_prodselececcionados clearfix">
    <table width="940" border="1">
      <tr style="border-bottom: solid 3px #fff;">
        <td class="tit_carrito">Producto</td>
        <td class="tit_carrito">Cantidad</td>
        <td class="tit_carrito">Precio</td>
        <td class="tit_carrito">Acciones</td>
      </tr>
    ';
    for ($i=0;$i<$this->num_productos;$i++){ 
      if($this->array_id_prod[$i]!=0){ 
        
        $id= $this->array_id_prod[$i];
        $cantidad = $this->array_cantidad_prod[$i];
        $tipo = $this->array_tipo[$i];
        $clase = $this->array_class[$i];
        $guest = $this->array_guest[$i];

        $totalProductos = (int)$totalProductos + (int)$cantidad;

        $mProducto = new Dbcategorias_productos();
        $mproducto = $mProducto->getByPk($id);

        $total = (int)$cantidad * (int)$mProducto->num_precio;

        $subtotal = $total + $subtotal;

        $iva = $subtotal * 0.16;

        $totalFinal = $subtotal;

        $printCarrito .= '
       <tr id="tr'.$i.'">
        <td class="nombreimagen_carrito">
          <img src="./img/'.$mProducto->imagen.'" width="50">
          <h3 class="tit_carrito2">
            '.$mProducto->txt_nombre.'
          </h3>
        </td>
        <td class="dec_carrito">
          <input type="text" class="cantidad_tablacarrito" id="nuevaCantidad_'.$i.'" type="text" name="cantidad" value="'.$cantidad.'"  onchange="actualizaCantidad('.$i.','.$cantidad.')">
        </td>
        <td class="dec_carrito">
          $ '.number_format($total).'
        </td>
        <td  class="dec_carrito">
          <a href="#" class="borrar inline" onclick="eliminarProductoCarrito('.$i.','.$totalProductos.')">
          </a>
        </td>
      </tr>
       ';

     }
    }


    $printCarrito .= '

    </table>
  </div>


  <div class="div_gris"></div>
  <div id="bloquetotales">
  <div class="condetallecompra clearfix">
    <div class="row">
      <div class="span8">
      </div>
      <div class="span4">

        <div class="row">
          <div class="span2">
            <h3 class="tit_detcompra">
              Total:
            </h3>
          </div>
          <div class="span2">
            <div class="val_compra">
              $ '.number_format($totalFinal).'
            </div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="row">
          <div class="span4">
            <a class="bt_vermas float_right carga_comprar" href="./index.php?module=emergente_pago&modal&total='.$totalFinal.'">
              Comprar
            </a>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>


    ';

    return $printCarrito;
  }

  public function bloqueTotales(){ 
    $subtotal = 0;
    $iva = 0;
    $totalFinal = 0;
    $printCarrito = '';
    for ($i=0;$i<$this->num_productos;$i++){ 
      if($this->array_id_prod[$i]!=0){ 
        
        $id= $this->array_id_prod[$i];
        $cantidad = $this->array_cantidad_prod[$i];
        $tipo = $this->array_tipo[$i];
        $clase = $this->array_class[$i];
        $guest = $this->array_guest[$i];

        $mProducto = new Dbcategorias_productos();
        $mproducto = $mProducto->getByPk($id);

        $total = (int)$cantidad * (int)$mProducto->num_precio;

        $subtotal = $total + $subtotal;

        $iva = $subtotal * 0.16;

        $totalFinal = $subtotal;

     }
    }


    $printCarrito .= '


  <div class="condetallecompra clearfix">
    <div class="row">
      <div class="span8">
      </div>
      <div class="span4">

       
        <div class="row">
          <div class="span2">
            <h3 class="tit_detcompra">
              Total:
            </h3>
          </div>
          <div class="span2">
            <div class="val_compra">
              $ '.number_format($totalFinal).'
            </div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="row">
          <div class="span4">
            <a class="bt_vermas float_right carga_comprar" href="./index.php?module=emergente_pago&modal&total='.$totalFinal.'">
              Comprar
            </a>
          </div>
        </div>
      </div>
    </div>
    </div>
    ';

    return $printCarrito;
  }


  function elimina_producto($linea){
    unset($this->array_id_prod[$linea]);
    unset($this->array_muestra_prod[$linea]);
    unset($this->array_cantidad_prod[$linea]);
    unset($this->array_tipo[$linea]);
    unset($this->array_class[$linea]);
    unset($this->array_guest[$linea]);
    $this->num_productos--;
  } 


  function acutalizar_cantidad($linea,$cantidad){
    $this->array_cantidad_prod[$linea] = $cantidad;
  }

  function guardar_carrito($ventas_id){

    for ($i=0;$i<$this->num_productos;$i++){
      if($this->array_id_prod[$i]!=0){
        
          $mOrdenes_productos = new Dbordenes_productos();

          $producto_codigo= $this->array_id_prod[$i];
          $cantidad_producto= $this->array_cantidad_prod[$i];

          $mOrdenes_productos->setordenes_id($ventas_id);
          $mOrdenes_productos->setcategorias_productos_id($producto_codigo);
          $mOrdenes_productos->setcantidad($cantidad_producto);

          $mOrdenes_productos->save();
          
      }
    } 
 }


}

?>