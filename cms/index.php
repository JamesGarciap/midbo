<?php
  
  session_start();
  ini_set('error_reporting', E_ALL);
  ini_set('display_errors', 1);

  header('Access-Control-Allow-Origin: *');

  // Arrancar buffer de salida
  ob_start();

  // Archivo de configuracion
  include( 'include/define.php' );
  include( 'include/config.php' );
  include( 'business/function/plGeneral.fnc.php' );

  // Manejador de errores
  ErrorHandler::SetHandler();


  $mLinkModuleDashboard = Link::ToSection( array( "s" => "dashboard" ) );
  $mLinkModuleHome = Link::ToSection( array( "s" => "home" ) );


  // Validamos la URL y redireccionamos si no es valida
  //Link::CheckRequest();

  // AJAX requests
  if ( isset( $_GET['ajax'] ) || isset( $_POST['ajax'] ) )
  {
    $archivo = StripHtml(GetData("myClass", "Ajax"));
    //var_dump( $_GET );
    $classAjax = new $archivo();
    eval('$classAjax->Func' . Link::CleanUrlText(StripHtml(GetData("myFunct", "Default"))) . '();');
  }
  else
  {
    if ( isset( $_GET["module"] ) )
    {
      if( "logout" == $_GET["module"] )
        $_SESSION["user"] = NULL;
    }

    // Incluimos la module espesificada
    //if( is_array( $_SESSION["user"] ) )
      $mConten = "storefront.php";
    //else
      //$mConten = "login.php";

    include 'modules/'.$mConten;
  }



  // Salida del contenido por el buffer -- POR SI NECESITAMOS CAMBIAR LAS CABECERAS PARA redireccionar y no tener contenido duplicado, mejora el SEO
  flush();
  ob_flush();
  ob_end_clean();
?>