<?php 
   
   $cContenidos_campos = new Dbcontenidos_campos();

   $mApuestas = $cContenidos_campos->getList(array('id_contenido' => 1 ));
   $mAntecedentes = $cContenidos_campos->getList(array('id_contenido' => 2 ));
   $mAlcances = $cContenidos_campos->getList(array('id_contenido' => 3 ));
   $mEquipo = $cContenidos_campos->getList(array('id_contenido' => 4 ));
   $mConvocatorias = $cContenidos_campos->getList(array('id_contenido' => 5 ));
   $mMuestra = $cContenidos_campos->getList(array('id_contenido' => 6 ));
   $mBasesMuestra = $cContenidos_campos->getList(array('id_contenido' => 7 ));
   $mItinerancia = $cContenidos_campos->getList(array('id_contenido' => 8 ));
   $mBasesItinerancia = $cContenidos_campos->getList(array('id_contenido' => 9 ));
   $mVoluntarios = $cContenidos_campos->getList(array('id_contenido' => 10 ));
   $mBasesVoluntarios = $cContenidos_campos->getList(array('id_contenido' => 11 ));
   $mAliados = $cContenidos_campos->getList(array('id_contenido' => 12 ));
   $mContacto = $cContenidos_campos->getList(array('id_contenido' => 13 ));
   $mInvitacionItinerancia = $cContenidos_campos->getList(array('id_contenido' => 14 ));


    function video_image($url){
        $image_url = parse_url($url);
        if($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com'){
            $array = explode("&", $image_url['query']);
            return "http://img.youtube.com/vi/".substr($array[0], 2)."/0.jpg";
        } else if($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com'){
            $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/".substr($image_url['path'], 1).".php"));
            return $hash[0]["thumbnail_large"];
        }
    }

 ?>