  <div class="cont_loading cont_tabla">
  <div class="loading_cont celda_tabla">
      <div class="cont_logoload inline">
        <div class="bg_logoload">
          <div class="personaje_logoload"></div>
        </div>
        <p>Cargando...</p>
      </div>
  </div>
</div>
<!-- <div class="bg_colibri"></div> -->

<div class="redes">
  <a href="https://www.facebook.com/MuestraInternacionalDocumental" target="_blank" class="opacidad">
    <img src="assets/img/redes/facebook.png">
  </a>
  <a href="https://twitter.com/muestradoc"  target="_blank"  class="opacidad">
    <img src="assets/img/redes/twiter.png">
  </a>
  <a href="https://www.youtube.com/user/Muestradoc"  target="_blank"  class="opacidad">
    <img src="assets/img/redes/youtube.png">
  </a>
<!--   <a href="#" class="opacidad">
    <img src="assets/img/redes/flicker.png">
  </a>
  <a href="#" class="opacidad">
    <img src="assets/img/redes/vimeo.png">
  </a> -->
</div>
<div class="degradado_midbo"></div>
<header class="header">
  <div class="header_arriba">
    <a href="#" class="logo_prin inline">
      <img src="assets/img/logo_midbo.png">
    </a>

    <a href="http://www.aladoscolombia.com/alados/" class="" target="_blank"  style="width: 180px;">
     <img src="./assets/img/logo_alados_enlace.png" style="width: 171px; float: right; margin: -10px;position: absolute;top: 42px;left: 730px;">
    </a>

    <div class="controles_fotos">
      <a href="#" class="bt_play inline control_activo">
        <img src="assets/img/iconos/play.png">
      </a>
      <a href="#" class="bt_pause inline">
        <img src="assets/img/iconos/pause.png">
      </a>
    </div>
    <a href="#" class="subecontenido opacidad">
      <img src="assets/img/bt_flechas.png">
    </a>
  </div>
  <div class="clear"></div>
  <nav class="nav_prin">
    <ul class="lista_navprin">
      <li class="li_navprin inline">
        <a href="0" class="bt_navprin bt_seccion" id="">       
          16Midbo
        </a>
        <ul class="ulsubnivel1">

          <li class="lisubnivel1">
            <a href="#" class="btsubnivel1 bt_seccion" id="bt_secant">
              Antecedentes
            </a>
<!--             <ul class="ulsubnivel2">
              <li class="lisubnivel2">
                <a href="#" class="btsubnivel2 bt_seccion" id="bt_sechist">
                  Historia
                </a>
              </li>

            </ul> -->
          </li>
          <li class="lisubnivel1">
            <a href="#" class="btsubnivel1 bt_seccion" id="bt_secgal">
              Galería
            </a>
          </li>
          <li class="lisubnivel1">
            <a href="#" class="btsubnivel1 bt_seccion" id="bt_secap">
              Desafíos
            </a>
          </li>  
          <li class="lisubnivel1">
            <a href="#" class="btsubnivel1 bt_seccion" id="bt_secalc">
              Equipo
            </a>
          </li>
        </ul>
      </li>
      <li class="li_navprin inline">
        <a href="0" class="bt_navprin bt_seccion" id="">        
          Convocatoria 2014
        </a>
        <ul class="ulsubnivel1">
          <li class="lisubnivel1">
            
            <a href="#" class="btsubnivel1 bt_seccion" id="bt_secnac">
              Invitación
            </a>

            <a href="#" class="btsubnivel1 bt_seccion" id="bt_secnacbases">
              Bases
            </a>

            <a href="#" class="btsubnivel1 bt_seccion" id="bt_secnacform">
              Formulario
            </a>


          </li>

        </ul>
      </li>


      <li class="li_navprin inline">
        <a href="#" class="bt_navprin bt_seccion" id="bt_secaliados">
          Aliados
        </a>
      </li>


     <li class="li_navprin inline">
        <a href="0" class="bt_navprin bt_seccion" id="">        
          Itinerancia
        </a>
        <ul class="ulsubnivel1">
          <li class="lisubnivel1">

            <a href="#" class="btsubnivel1 bt_seccion" id="bt_secitenerinvitacion">
              Invitación
            </a>
            <a href="#" class="btsubnivel1 bt_seccion" id="bt_secitenerbases">
              Bases
            </a>

            <a href="#" class="btsubnivel1 bt_seccion" id="bt_secnacformitc">
              Formulario
            </a>

          </li>
        </ul>
      </li>

      <li class="li_navprin inline">
        <a href="#" class="bt_navprin bt_seccion" id="bt_seccionnoticias">
          Noticias
        </a>
      </li>



      <li class="li_navprin inline">
        <a href="#" class="bt_navprin bt_seccion" id="bt_seccontacto">
          Contacto
        </a>
      </li>
      <!-- 
      <li class="li_navprin inline">
        <a href="http://www.aladoscolombia.com/alados/" class="bt_navprin " target="_blank"  style="width: 180px;">
        <img src="./assets/img/logo_alados_enlace.png" style="width: 40px; float: left; margin: -10px;">
        </a>
      </li> -->

    </ul>
  </nav>
  
</header>

<div class="vertical-layout">
<div class="bg_colibri"></div>

<section class="contenedor contenidos_tabs" id="secmidbo">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
              <?php echo $mApuestas[0]['txt_valor'] ?>

    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="secitenerinvitacion">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
              <?php echo utf8_encode($mInvitacionItinerancia[0]['txt_valor']) ?>

    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="secap">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
              <?php echo $mApuestas[0]['txt_valor'] ?>

    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="secant">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
              <?php echo $mAntecedentes[0]['txt_valor'] ?>

    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="sechist">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
              <?php echo $mAntecedentes[0]['txt_valor'] ?>

    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="seccionnoticias">
  <div class="cont_contenidos">
    <div class="contenidosvacio clearfix">
      <div class="cont_buscador">
        <form>
          <input type="text" id="valorvoces" value="">
          <button type="button" class="btn bt_buscar"></button>
        </form>
        
      </div>
      
      <div class="clear"></div>
      <div class="contenidosvacio clearfix">


      <?php 

         $cNoticias = new Dbnoticias();
         $mNoticias = $cNoticias->getList(array('estado' => 1 ));        
        
      ?>
      <?php if ($mNoticias): ?>

      <?php foreach ($mNoticias as $noticiascont): ?>
       
      <div class="detalle_noticia" id="noticia<?php echo $noticiascont['id'] ?>">
        <h2 class="tit_modal"><?php echo $noticiascont['txt_nombre'] ?></h2>
        <div class="clear"></div>
        <img src="./img/<?php echo $noticiascont['file_imagen'] ?>">
        <p><?php echo $noticiascont['txt_descripcion'] ?></p>
      </div>

      <?php if ($noticiascont['tipo']==1): ?>
        
        <div class="modulo modulo_soloimagen">
          <img src="./img/<?php echo $noticiascont['file_imagen'] ?>">
          <h2 class="tit_imagen"><?php echo $noticiascont['txt_nombre'] ?></h2>
          <a href="#noticia<?php echo $noticiascont['id'] ?>" class="ver_masmodulo fancybox">
            <div class="cont_tabla">
              <div class="celda_tabla">
                <div class="inline">
                  <img src="assets/img/iconos/ver_masmodulo.png">
                  <div class="clear"></div>
                  <p>ver más</p>
                </div>
              </div>
            </div>
          </a>
        </div>
        
        <?php else:  ?>

        <div class="modulo modulo_video">
          <img src="<?php echo video_image($noticiascont['url']); ?>">
          <h2 class="tit_imagen"><?php echo $noticiascont['txt_nombre'] ?></h2>
          <a href="<?php echo $noticiascont['url'] ?>" class="ver_masmodulovideo fancybox-media">
            <div class="cont_tabla">
              <div class="celda_tabla">
                <div class="inline">
                  <img src="assets/img/iconos/video.png">
                  <div class="clear"></div>
                  <p>ver video</p>
                </div>
              </div>
            </div>
          </a>
        </div>


      <?php endif ?>
      <?php endforeach ?>
      <?php endif ?>

      </div>
    </div>
  </div>
</section>


<section class="contenedor contenidos_tabs" id="secgal">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
      <div class="cont_galeria" id="cont_afiches">
        <a href="assets/img/afiches/big/1.jpg" class="afiche" data-fancybox-group="gallery" title="Titulo">
          <img src="assets/img/afiches/thumb/1.jpg">
        </a>
        <a href="assets/img/afiches/big/2.jpg" class="afiche" data-fancybox-group="gallery" title="Titulo">
          <img src="assets/img/afiches/thumb/2.jpg">
        </a>
        <a href="assets/img/afiches/big/3.jpg" class="afiche" data-fancybox-group="gallery" title="Titulo">
          <img src="assets/img/afiches/thumb/3.jpg">
        </a>
        <a href="assets/img/afiches/big/4.jpg" class="afiche" data-fancybox-group="gallery" title="Titulo">
          <img src="assets/img/afiches/thumb/4.jpg">
        </a>
        <a href="assets/img/afiches/big/5.jpg" class="afiche" data-fancybox-group="gallery" title="Titulo">
          <img src="assets/img/afiches/thumb/5.jpg">
        </a>
        <a href="assets/img/afiches/big/6.jpg" class="afiche" data-fancybox-group="gallery" title="Titulo">
          <img src="assets/img/afiches/thumb/6.jpg">
        </a>
        <a href="assets/img/afiches/big/7.jpg" class="afiche" data-fancybox-group="gallery" title="Titulo">
          <img src="assets/img/afiches/thumb/7.jpg">
        </a>
        <a href="assets/img/afiches/big/8.jpg" class="afiche" data-fancybox-group="gallery" title="Titulo">
          <img src="assets/img/afiches/thumb/8.jpg">
        </a>
        <a href="assets/img/afiches/big/9.jpg" class="afiche" data-fancybox-group="gallery" title="Titulo">
          <img src="assets/img/afiches/thumb/9.jpg">
        </a>
        <a href="assets/img/afiches/big/10.jpg" class="afiche" data-fancybox-group="gallery" title="Titulo">
          <img src="assets/img/afiches/thumb/10.jpg">
        </a>
        <a href="assets/img/afiches/big/11.jpg" class="afiche" data-fancybox-group="gallery" title="Titulo">
          <img src="assets/img/afiches/thumb/11.jpg">
        </a>
        <a href="assets/img/afiches/big/12.jpg" class="afiche" data-fancybox-group="gallery" title="Titulo">
          <img src="assets/img/afiches/thumb/12.jpg">
        </a>
        <a href="assets/img/afiches/big/12.jpg" class="afiche" data-fancybox-group="gallery" title="Titulo">
          <img src="assets/img/afiches/thumb/13.jpg">
        </a>
      </div>
    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="secalc">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
       <?php echo $mAlcances[0]['txt_valor'] ?>
    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="secconv">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
       <?php echo $mConvocatorias[0]['txt_valor'] ?>
    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="secnac">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
       <?php echo $mMuestra[0]['txt_valor'] ?>
    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="secnacbases">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
       <?php echo $mBasesMuestra[0]['txt_valor'] ?>
    </div>
  </div>
</section>




<section class="contenedor contenidos_tabs" id="secnacform">
  <div class="cont_contenidos">
    <div class="contenidos contenidos_form clearfix">
      <h2 class="tit">FORMULARIO DE INSCRIPCIÓN</h2>
      <p class="tit">INFORMACIÓN DE LA OBRA</p>

<!--       <p>Los títulos en el idioma original y en ingles son obligatorios. El titulo en español es opcional y si no se incluye la organización se encargará de su traducción.</p>
 -->
      <form class="form-horizontal">
        <fieldset>
          <div class="control-group">
            <label class="control-label" for="">Título original:</label>
              <div class="controls">
                <input type="text" id="ins_t_original">
              </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="">Título en Español:</label>
            <div class="controls">
              <input type="text" id="ins_t_espanol">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="">Duración:</label>
            <div class="controls">
              <input type="text" id="ins_duracion">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="">Año de Producción:</label>
            <div class="controls">
              <input type="text" id="ins_a_produccion">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="">Idioma original:</label>
            <div class="controls">
              <input type="text" id="ins_idioma">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="">Subtítulos:</label>
            <div class="controls">
              <input type="text" id="ins_subtitulos">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="">Formato original:</label>
            <div class="controls">
              <input type="text" id="ins_formato_original">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="">Formato de exhibición:</label>
            <div class="controls">
              <input type="text" id="ins_formato_exhibicion">
            </div>
          </div>
        </fieldset>
        <fieldset>
           <p class="tit">FICHA TÉCNICA</p>
            <div class="control-group">
              <label class="control-label" for="">Director(es):</label>
                <div class="controls">
                  <input type="text" id="ins_directores">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">Productor(es):</label>
                <div class="controls">
                  <input type="text" id="ins_productores">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">Fotografía:</label>
                <div class="controls">
                  <input type="text" id="ins_fotografia">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">Sonido:</label>
                <div class="controls">
                  <input type="text" id="ins_sonido">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">Montaje:</label>
                <div class="controls">
                  <input type="text" id="ins_montaje">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">Música:</label>
                <div class="controls">
                  <input type="text" id="ins_musica">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">Otros:</label>
                <div class="controls">
                  <input type="text" id="ins_otros">
                </div>
            </div>
        </fieldset>
        <fieldset>
            <div class="control-group">
              <label class="control-label">Sinopsis (Máximo 400 caracteres) :</label>
              <div class="controls">
                <textarea rows="3" id="ins_sinopsis"></textarea>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Premios y festivales (Nombre, Festival, País, Año):</label>
              <div class="controls">
                <textarea rows="3" id="ins_premios"></textarea>
              </div>
            </div>
        </fieldset>
        <fieldset>
          <p class="tit">INFORMACIÓN DE CONTACTO </p>
            <div class="control-group">
              <label class="control-label" for="">Nombre:</label>
                <div class="controls">
                  <input type="text" id="ins_nombre">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">Cargo:</label>
                <div class="controls">
                  <input type="text" id="ins_cargo">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">Identificación:</label>
                <div class="controls">
                  <input type="text" id="ins_identificacion">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">Dirección:</label>
                <div class="controls">
                  <input type="text" id="ins_direccion">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">Ciudad:</label>
                <div class="controls">
                  <input type="text" id="ins_ciudad">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">País:</label>
                <div class="controls">
                  <input type="text" id="ins_pais">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">Teléfono:</label>
                <div class="controls">
                  <input type="text" id="ins_telefono">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">Celular:</label>
                <div class="controls">
                  <input type="text" id="ins_celular">
                </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="">E-mail:</label>
                <div class="controls">
                  <input type="text" id="ins_email">
                </div>
            </div>
        </fieldset>
        <fieldset>
           <p>He leído y por lo tanto acepto las CONDICIONES GENERALES de participación en la 16ª Muestra Internacional Documental, MIDBO 2014. Declaro con el envío de esta ficha de inscripción que soy propietario de todos los derechos del documental inscrito, de sus archivos sonoros y/o  visuales, y exonero a la Organización de cualquier reclamación que puedan hacer terceros sobre la exhibición pública, la propiedad intelectual o patrimonial de los mismos en el marco de la 16ª Muestra Internacional Documental.</p>
           <div class="control-group">

              <label class="checkbox inline">
                <input type="checkbox" id="terminos" value=""> Acepto
              </label>
        </fieldset>

        <button type="button" class="btn" id="submitformIns">Enviar</button>
      </form>
    </div>
    <div class="clear"></div>
  </div>
</section>
<section class="contenedor contenidos_tabs" id="secnacformitc">
  <div class="cont_contenidos">
    <div class="contenidos contenidos_form clearfix">
      <form class="form-horizontal">
        <fieldset>
          <div class="control-group">
            <label class="control-label" for="">Nombre:</label>
              <div class="controls">
                <input type="text" id="itn_nombre">
              </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="">Organización:</label>
            <div class="controls">
              <input type="text" id="itn_organizacion">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="">E-mail:</label>
            <div class="controls">
              <input type="text" id="itn_email">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="">Teléfono:</label>
            <div class="controls">
              <input type="text" id="itn_telefono">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="">Celular:</label>
            <div class="controls">
              <input type="text" id="itn_celular">
            </div>
          </div>
          <div class="control-group">
            <label class="control-label">En unas pocas líneas coméntenos su interés en hacer parte de la itinerancia de MIDBO 2014:</label>
            <div class="controls">
              <textarea rows="3" id="itn_comentario"></textarea>
            </div>
          </div>


        </fieldset>
    

        <button type="button" class="btn" id="submitformItn">Enviar</button>
      </form>
    </div>
    <div class="clear"></div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="secitener">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
       <?php echo $mItinerancia[0]['txt_valor'] ?>
    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="secitenerbases">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
       <?php echo $mBasesItinerancia[0]['txt_valor'] ?>
    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="secvol">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
       <?php echo $mVoluntarios[0]['txt_valor'] ?>
    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="secvolbases">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
       <?php echo $mBasesVoluntarios[0]['txt_valor'] ?>
    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="secaliados">
  <div class="cont_contenidos">
    <div class="contenidos clearfix">
       <?php echo $mAliados[0]['txt_valor'] ?>
    </div>
  </div>
</section>

<section class="contenedor contenidos_tabs" id="seccontacto">
  <div class="cont_contenidos">
    <div class="contenidos contenidos_form clearfix">
      <form class="form-horizontal">
        <div class="control-group">
          <label class="control-label" for="inputNombre">Nombre:</label>
            <div class="controls">
              <input type="text" id="con_nombre">
            </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="inputApellido">Apellido:</label>
          <div class="controls">
            <input type="text" id="con_apellido">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label" for="inputEmail">Email:</label>
          <div class="controls">
            <input type="text" id="con_email">
          </div>
        </div>
        <div class="control-group">
          <label class="control-label">Mensaje:</label>
          <div class="controls">
            <textarea rows="3" id="con_comentario"></textarea>
          </div>
        </div>
        <button type="button" class="btn" id="submitformCon">Enviar</button>
      </form>
    </div>
    <div class="clear"></div>
     <div class="text_contacto clearfix">
          
        <?php echo $mContacto[0]['txt_valor'] ?>

     </div>
  </div>
</section>

</div>