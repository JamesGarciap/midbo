
<!DOCTYPE>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js ie9">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<title>16 MIDBO</title>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=1024, maximum-scale=2">
<meta http-equiv="content-language" content="es" />
<meta http-equiv="pragma" content="No-Cache" />
<meta name="Keywords" lang="es" content="" />
<meta name="Description" content="Abierta la convocatoria de la 16ª Muestra Internacional Documental de Bogotá, MIDBO 2014." />
<meta name="copyright" content="gurus.com" />
<meta name="date" content="2013" />
<meta name="author" content="diseño web: gurus.com" />
<meta name="robots" content="All" />


<!-- fb -->
<meta property="og:title" content="16 MIDBO"/>
<meta property="og:url" content="http://www.muestradoc.com/midbo/"/>
<meta property="og:image" content="http://www.muestradoc.com/midbo/img/midbo_fb.jpg"/>
<meta property="og:description" content="Abierta la convocatoria de la 16ª Muestra Internacional Documental de Bogotá, MIDBO 2014."/> 


<!-- Estilos -->

<!-- Fuentes
================================================== -->
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,700italic,400italic,300,300italic' rel='stylesheet' type='text/css'>

<!-- Bootstrap -->
<link href="assets/js/lib/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="assets/js/lib/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" type="text/css" />

<!-- UI
================================================== -->
<link rel="stylesheet" href="assets/js/lib/jquery/css/ui-lightness/jquery-ui-1.9.1.custom.css" rel="stylesheet" type="text/css">

<!-- mCustomScrollbar
================================================== -->
<link rel="stylesheet" href="assets/js/lib/custom-scrollbar/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">

<!-- vegas
================================================== -->
<link rel="stylesheet" href="assets/js/lib/vegas/jquery.vegas.min.css" rel="stylesheet" type="text/css">

<!-- Fancybox
================================================== -->
<link rel="stylesheet" type="text/css" href="assets/js/lib/source/jquery.fancybox.css"/>
<link rel="stylesheet" type="text/css" href="assets/js/lib/source/helpers/jquery.fancybox-thumbs.css"/>


<!-- General
================================================== -->

<link href="assets/css/joss.css" rel="stylesheet" type="text/css" />
<link href="assets/css/midbo.css" rel="stylesheet" type="text/css" />

</head>
<body>
  
    <?php


      $mConten = "home.php";

      $mSeccion = GetData( "module", FALSE );

      if( FALSE !== $mSeccion )
      {
        $mConten = Link::CleanUrlText( $mSeccion );

        //Si existe subseccion, concatenamos para generar el nombre del archivo
        $mSubseccion = GetData( "option", FALSE );

        if( FALSE !== $mSubseccion )
        {
          $mConten .= "/".Link::CleanUrlText( $mSubseccion );
        }

        // Si existe accion, concatenamos para generar el nombre del archivo
        $mAccion = GetData( "accion", FALSE );

        if( FALSE !== $mAccion )
        {
          $mConten .= "_".Link::CleanUrlText( $mAccion );
        }

        $mConten = str_replace( "-", "_", $mConten ) . ".php";
      }

      $mModal = GetData( "modal", FALSE );

      if( FALSE === $mModal )
      {
        //include( SITE_ROOT."sections/preheader.php");
    ?>
     
    	  <?php include( SITE_ROOT."sections/header.php"); ?>
          <?php
            include_once( SITE_ROOT.'sections/'.$mConten );
          ?>


    <?php
        include( SITE_ROOT."sections/footer.php" );
      }
      else
      {
        include_once( SITE_ROOT.'sections/'.$mConten );
      }
    ?>


 <!-- Scripts -->
 <script src="assets/js/lib/jquery-1.8.3.min.js"></script>
  

  <!-- Bootstrap
  ================================================== -->
  <script src="assets/js/lib/bootstrap/js/bootstrap-transition.js"></script>
  <script src="assets/js/lib/bootstrap/js/bootstrap-alert.js"></script>
  <script src="assets/js/lib/bootstrap/js/bootstrap-modal.js"></script>
  <script src="assets/js/lib/bootstrap/js/bootstrap-dropdown.js"></script>
  <script src="assets/js/lib/bootstrap/js/bootstrap-scrollspy.js"></script>
  <script src="assets/js/lib/bootstrap/js/bootstrap-tab.js"></script>
  <script src="assets/js/lib/bootstrap/js/bootstrap-tooltip.js"></script>
  <script src="assets/js/lib/bootstrap/js/bootstrap-popover.js"></script>
  <script src="assets/js/lib/bootstrap/js/bootstrap-button.js"></script>
  <script src="assets/js/lib/bootstrap/js/bootstrap-collapse.js"></script>
  <script src="assets/js/lib/bootstrap/js/bootstrap-carousel.js"></script>
  <script src="assets/js/lib/bootstrap/js/bootstrap-typeahead.js"></script> 
  <script src="assets/js/lib/bootstrap/js/bootstrap-fileupload.js"></script> 


<!-- Greensock
================================================== -->
<script src="assets/js/lib/greensock/uncompressed/TweenMax.js"></script>
<script src="assets/js/lib/greensock/uncompressed/jquery.gsap.js"></script>


<!-- Greensock
================================================== -->
<script src="assets/js/lib/jquery/jquery-ui-1.9.1.custom.min.js"></script>
<script src="assets/js/lib/jquery/jquery.ui.touch-punch.js"></script>

 <!-- custom-scrollbar
================================================== -->
<script src="assets/js/lib/custom-scrollbar/js/minified/jquery.mCustomScrollbar.concat.min.js"></script>

 <!-- vegas
================================================== -->
<script src="assets/js/lib/vegas/jquery.vegas.js"></script>

 <!-- isotope
================================================== -->
<script src="assets/js/lib/isotope/js/isotope-docs.js"></script>

  <!-- Fancybox
================================================== -->

  <script type="text/javascript" src="assets/js/lib/source/jquery.fancybox.js"></script>

  <!-- Add Button helper (this is optional) -->
  <script type="text/javascript" src="assets/js/lib/source/helpers/jquery.fancybox-buttons.js"></script>

  <!-- Add Thumbnail helper (this is optional) -->
  <script type="text/javascript" src="assets/js/lib/source/helpers/jquery.fancybox-thumbs.js"></script>

  <!-- Media (this is optional) -->
  <script type="text/javascript" src="assets/js/lib/source/helpers/jquery.fancybox-media.js"></script>


<!-- Acciones
================================================== -->  
 <script src="assets/js/midbo.js"></script>

 <script type="text/javascript">

$(window).load(function() {


 $.vegas('slideshow', {
        delay:5000,         // Indica el tiempo que se muestra una imagen
        backgrounds:[       // imagenes a mostrar
            { src:'assets/img/bg/1.jpg',fade:1000 },
            { src:'assets/img/bg/2.jpg',fade:1000 },
            { src:'assets/img/bg/3.jpg',fade:1000 },
            { src:'assets/img/bg/4.jpg',fade:1000 },
            { src:'assets/img/bg/5.jpg',fade:1000 },
            { src:'assets/img/bg/6.jpg',fade:1000 },
            { src:'assets/img/bg/7.jpg',fade:1000 },
            { src:'assets/img/bg/8.jpg',fade:1000 },
            { src:'assets/img/bg/9.jpg',fade:1000 },

        ]
    })('overlay', {
        src:'imagenes/01.png' // Imagen que se superpone sobre las imágenes de fondo
    });

var $container = $('#cont_afiches').isotope({
  // main isotope options
  itemSelector: '.afiche'
});

$('a#bt_secgal').click(function (e) {
e.preventDefault();
    setTimeout(function(){$container.isotope('layout')}, 1000);
  });

$('.afiche').fancybox();


});


</script>



</body>
</html>

