$(window).load(function() {

/*Load*/

function loading_general(){
    $(".cont_loading").fadeOut(500);
}

loading_general();
  
  /*Header*/

function inicio() {
  var inicioTimeline = new TimelineLite();
      inicioTimeline.to($(".nav_prin, .redes, .controles_fotos"), 0.1, {css:{display:'block'}}) 
                    .to($(".nav_prin, .redes, .controles_fotos"), 1, {css:{opacity:'1'}}) 
      return inicioTimeline;                 
}

function subecontenido() {
  var subecontenidoTimeline = new TimelineLite();
      subecontenidoTimeline.to($(".vertical-layout"), 0.1, {css:{display:'block'}}) 
                   .to($(".vertical-layout"), 1, {css:{opacity:'1'}}) 
                   .to($(".controles_fotos, .derechos"), 1, {css:{opacity:'0'}}) 
                   .to($(".controles_fotos, .derechos"), 0.1, {css:{display:'none'}}) 
                   .to($("a.subecontenido"), 0.1, {css:{display:'block'}}) 
                   .to($("a.subecontenido"), 1, {css:{opacity:'0.4'}}) 
      return subecontenidoTimeline;                 
}

function regresagaleria() {
  var regresagaleriaTimeline = new TimelineLite();
      regresagaleriaTimeline.to($("a.subecontenido, .vertical-layout"), 1, {css:{opacity:'0'}}) 
                   .to($("a.subecontenido, .vertical-layout"), 0.1, {css:{display:'none'}}) 
                   .to($(".controles_fotos, .derechos"), 0.1, {css:{display:'block'}})
                   .to($(".controles_fotos, .derechos"), 1, {css:{opacity:'1'}})               
      return regresagaleriaTimeline;                 
}

$('a.logo_prin').click(function (e) {
    e.preventDefault(); 
       inicio();
});

$('a.bt_play').click(function (e) {
    e.preventDefault(); 
      $(".controles_fotos a").removeClass("control_activo");
      $(this).addClass("control_activo");
      $.vegas( 'slideshow' );   // Inicia la reproduccion
});

$('a.bt_pause').click(function (e) {
    e.preventDefault(); 
      $(".controles_fotos a").removeClass("control_activo");
      $(this).addClass("control_activo");
      $.vegas('pause');
});




//anclas

//tabs general


$('a.bt_seccion').click(function (e) {
e.preventDefault(); 

  subecontenido();
  
  var id_bttab = $(this).attr("id")
  var str=id_bttab;
  var n=str.split("bt_");
  var new_name = "#" + n;
  var patron=",";
  new_name=new_name.replace(patron,'')
  
  console.log(new_name);
  
  $('.contenidos_tabs').fadeOut(500);
  $(new_name).fadeIn(500);
  });

$('a.bt_navprin.bt_seccion').click(function (e) {
e.preventDefault(); 
  $('a.bt_navprin').removeClass("bt_navprinactivo");
  $('a.btsubnivel1').removeClass("bt_subactivo");
  $('a.btsubnivel2').removeClass("bt_subactivo");
  $(this).addClass("bt_navprinactivo");
});

$('a.btsubnivel1').click(function (e) {
e.preventDefault(); 
  $('a.bt_navprin').removeClass("bt_navprinactivo");
  $(this).parent("li").parent("ul").parent("li").find('a.bt_navprin').addClass("bt_navprinactivo");
  $('a.btsubnivel1').removeClass("bt_subactivo");
  $('a.btsubnivel2').removeClass("bt_subactivo");
  $(this).addClass("bt_subactivo");
});
$('a.btsubnivel2').click(function (e) {
e.preventDefault(); 
  $('a.bt_navprin').removeClass("bt_navprinactivo");
  $(this).parent("li").parent("ul").parent("li").parent("ul").parent("li").find('a.bt_navprin').addClass("bt_navprinactivo");
  $('a.btsubnivel1').removeClass("bt_subactivo");
  $(this).parent("li").parent("ul").parent("li").find('a.btsubnivel1').addClass("bt_subactivo");
  $('a.btsubnivel2').removeClass("bt_subactivo");
  $(this).addClass("bt_subactivo");
});

$('a.subecontenido').click(function (e) {
e.preventDefault(); 
  regresagaleria();
  $('a.bt_navprin').removeClass("bt_navprinactivo");
  $('a.btsubnivel1').removeClass("bt_subactivo");
  $('a.btsubnivel2').removeClass("bt_subactivo");
});



/*Modales*/

$('.fancybox').fancybox();

  $('.fancybox-media')
        .attr('rel', 'media-gallery')
        .fancybox({
          openEffect : 'none',
          closeEffect : 'none',
          prevEffect : 'none',
          nextEffect : 'none',

          arrows : false,
          helpers : {
            media : {},
            buttons : {}
          }
  });


});




      $(document).ready(function(){
       
          function validar_email2(valor)
          {
              // creamos nuestra regla con expresiones regulares.
              var filter = /[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
              // utilizamos test para comprobar si el parametro valor cumple la regla
              if(filter.test(valor))
                  return true;
              else
                  return false;
          }

          // Formulario de inscripción
          $("#submitformIns").click(function()
          {


              if($("#ins_email").val() == ''){
                  alert("Ingrese un email");
              }
              else if(validar_email2($("#ins_email").val())){

                if (!$('#terminos').attr('checked')) {
                    alert("Debe aceptar los términos y condiciones para continuar");
                }
                else{                 

                $.post('./include/PHPMailer/sends/sendInsForm.php', {

                  ins_t_original: $("#ins_t_original").val(),
                  ins_t_espanol: $("#ins_t_espanol").val(),
                  ins_duracion: $("#ins_duracion").val(),
                  ins_a_produccion: $("#ins_a_produccion").val(),
                  ins_idioma: $("#ins_idioma").val(),
                  ins_subtitulos: $("#ins_subtitulos").val(),
                  ins_formato_original: $("#ins_formato_original").val(),
                  ins_formato_exhibicion: $("#ins_formato_exhibicion").val(),
                  ins_directores: $("#ins_directores").val(),
                  ins_productores: $("#ins_productores").val(),
                  ins_fotografia: $("#ins_fotografia").val(),
                  ins_sonido: $("#ins_sonido").val(),
                  ins_montaje: $("#ins_montaje").val(),
                  ins_musica: $("#ins_musica").val(),
                  ins_otros: $("#ins_otros").val(),
                  ins_sinopsis: $("#ins_sinopsis").val(),
                  ins_premios: $("#ins_premios").val(),
                  ins_nombre: $("#ins_nombre").val(),
                  ins_cargo: $("#ins_cargo").val(),
                  ins_identificacion: $("#ins_identificacion").val(),
                  ins_direccion: $("#ins_direccion").val(),
                  ins_ciudad: $("#ins_ciudad").val(),
                  ins_pais: $("#ins_pais").val(),
                  ins_telefono: $("#ins_telefono").val(),
                  ins_celular: $("#ins_celular").val(),
                  ins_email: $("#ins_email").val()

                  });


                  alert("Su información de inscripción se ha enviado correctamente");


                }
              }else
              {

              
                  alert("El email no es valido");
              }



          });


          // Formulario itinerancia nacional
          $("#submitformItn").click(function()
          {


              if($("#itn_email").val() == ''){
                  alert("Ingrese un email");
              }
              else if(validar_email2($("#itn_email").val())){

                $.post('./include/PHPMailer/sends/sendItnForm.php', {

                  itn_nombre: $("#itn_nombre").val(),
                  itn_organizacion: $("#itn_organizacion").val(),
                  itn_email: $("#itn_email").val(),
                  itn_telefono: $("#itn_telefono").val(),
                  itn_celular: $("#itn_celular").val(),
                  itn_comentario: $("#itn_comentario").val(),
                  
                  });


                  alert("Su información de inscripción a Itinerancia se ha enviado correctamente");


                
              }else
              {

              
                  alert("El email no es valido");
              }



          });

          // Formulario de contacto
          $("#submitformCon").click(function()
          {


              if($("#con_email").val() == ''){
                  alert("Ingrese un email");
              }
              else if(validar_email2($("#con_email").val())){

                $.post('./include/PHPMailer/sends/sendContactUs.php', {

                  con_nombre: $("#con_nombre").val(),
                  con_apellido: $("#con_apellido").val(),
                  con_email: $("#con_email").val(),
                  con_comentario: $("#con_comentario").val(),

                  });

                  alert("Su información de contacto se ha enviado correctamente");


                
              }else
              {

              
                  alert("El email no es valido");
              }



          });


       
      });




